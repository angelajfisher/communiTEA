<div align="center">

# CommuniTEA

Bringing your local community together over a cuppa.

[Visit Deployed Site](https://angelajfisher.gitlab.io/communiTEA) | [View Interactive API](https://www.angelajfisher.com/projects/communitea/api/docs)

</div>

## Team

- Angela Fisher (Team lead; Wiki)
- Laura Le (Events)
- Alvin Mach (Users)


## Design

CommuniTEA is built with FastAPI (Python) on the backend, React (JavaScript) on the frontend, and utilizes a PostgreSQL database.

- [API design](docs/api.md)
- [Data models](docs/data-model.md)
- [GHI wireframes](docs/wireframe.md)

## Intended market

We are targeting tea enthusiasts who want to join a like-minded community to learn and explore various teas. Our aim is to cater to individuals who have a passion for tea, offering them opportunities to gain more knowledge about tea, locate businesses to purchase specific teas, and attend or host their own tea events. In parallel, business users can also display their store information and offered teas. They can also attend and host their own events.

## Functionality

- Individuals can sign up for a regular user account.

  - They can view teas in the wikiTEAdia to read about teas and also find business locations where these teas are offered.
  - They can add teas from the wikiTEAdia to their favorites list.
  - They can leave a review on the business profile.
  - They can also see all upcoming events, attend and event host their own events.

- Businesses can sign up for a business account

  - They can have a profile to display store details along with their tea offerings.
  - They can also request to add or make changes to exisiting teas to the wikiTEAdia.
  - In addition, they can attend or host their own events.

- Admin users can add new teas to the wikiTEAdia. They can review or approve tea submissions proposed by business accounts.

## Project Initialization

To fully enjoy this application on your local machine, we strongly recommend using [Docker](https://www.docker.com/). Once that is installed, follow these steps to run the project:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run `docker volume create postgres-data`
4. Run `docker compose build`
5. Run `docker compose up`
6. After waiting some time for docker to finish initializing everything, open your browser and navigate to:
   1. For the frontend: http://localhost:3000/
   2. For the backend: http://localhost:8000/docs

## Contribution Guidelines

Thank you so much for your interest in CommuniTEA! While we are not open to code contributions at this time, we greatly value any feedback you may have regarding this project. Please feel free to open an issue with a detailed description of a bug or feature request and we will evaluate and act on your suggestion accordingly.
