## 9-18-23 through 9-22-23

### The first week of project time, I worked on:

- Deciding on what type of application with my group
- Discovering Figma and learning how to use it for our wireframes
- Wireframing our project with my group
- Our group presented the project to the class and got a good feedback on our application idea

**AH-HA!💡**
Learning how fun and useful Figma is!

**🎉 Celebrations 🎉**
Our team decided on a project and successfully created our wireframes together!

### Bugs encountered 🐛🐞🐜 :

🪲 Our wireframes didn't have all the pages we needed.

**\~Solution~**
We added more wireframes. :)

### References Used:

The wireframe Learn documentation: https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/04-unit-tests/70-api-design.md

### Any Blockers:

No blockers as of now!

### Tomorrow I'm working on:

Finishing wireframes and creating our draft endpoints!

---

## 9/25/23

### Today I worked on:

- Presenting the wireframes, completed endpoints and tables to Rosheen!
- Then adjusting our application to her notes/tips
- Also created a bunch of issues for our endpoints in Gitlab!

**AH-HA!💡**
Rosheen let us know that we didn’t need to create two users (business and regular user). We can just create one! We didn’t know that we could create users and have different functionalities. That was very helpful. We also learned today that we should not add ‘create’ in our post endpoint paths so we removed that.

**🎉 Celebrations 🎉**
We got approval and help on our wireframes + endpoints! That’s definitely worth celebrating!

### Bugs encountered 🐛🐞🐜 :

🪲 We were adding ‘create’ on our endpoints and that was not correct!

**\~Solution~**
We modified our endpoints. :)

### References Used Today:

The wireframe Learn documentation: https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/04-unit-tests/70-api-design.md

### Any Blockers:

No blockers as of yet!

### Tomorrow I'm working on:

Creating issues in Gitlab!

### References for Tomorrow:

Learn documentation on creating issues/user stories:
https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/05-sql-I/68-user-stories.md

---

## 9-26-23

### Today I worked on:

- Learning how to do merge requests
- Setting up the project. We all worked on the Learn instructions to set up the project individually, with postgres and pgadmin.
- Then we chose one person to merge the changes into main on Gitlab.

**AH-HA!💡**
Learning how to create the merge requests and getting more comfortable with Gitlab!

**🎉 Celebrations 🎉**
We made a successful merge into main. :)

### Bugs encountered 🐛🐞🐜 :

🪲 One of our team members was having an issue with pulling from main due to uncommitted changes on their local main branch. Uncommitted changes were then pushed to main and we had to revert that commit.

**\~Solution~**
We all worked through it together and we were able to get through it and resolve the issue! :)

### References Used Today:

Jordan’s very detailed post (in our live channel) on workflow, how to create issues, and how to merge!! So helpful!

### Any Blockers:

The issue with pulling from main was a huge blocker, but thankfully we all got through it as a team!

### Tomorrow I'm working on:

Coding our authenticator.py!

### References for Tomorrow:

Backend auth documentation from Learn:
https://learn-2.galvanize.com/cohorts/3733/blocks/1897/content_files/build/02-backend-auth/01-using-jwtdown-fastapi.md

---

## 9-27-23

### Today I worked on:

- Making a Gitlab issue for migrations
- Created a draft merge request
- Created the migrations file named 001_create_tables.py and entered in our SQL statements
- We then tested it in pgadmin and beekeeper
- The models were complete and we were ready to go!
- I commited it to main and then a team member approved the MR.
- Then I merged it to main.
- Started on authentication

**AH-HA!💡**
I learned more about SQL migrations.

**🎉 Celebrations 🎉**
We successfully made our tables!

### Bugs encountered 🐛🐞🐜 :

🪲 We encountered issues with our authentication.

**\~Solution~**
We don't hacve a solution just yet!

### References Used Today:

https://jwtdown-fastapi.readthedocs.io/en/latest/intro.html

### Any Blockers:

Authentication is not working yet!

### Tomorrow I'm working on:

Completing Auth!

### References for Tomorrow:

https://jwtdown-fastapi.readthedocs.io/en/latest/intro.html

---

## 9-28-23

### Today I worked on:

- Figuring out how to code our Authentication! We finally got it to work! I created the merge request, got it approved by a team member and merged it into main!
- I got started on my first individual issue which is posting an event page!

**AH-HA!💡**
I learned more about how auth works!!

**🎉 Celebrations 🎉**
We successfully made our authentication code work!

### Bugs encountered 🐛🐞🐜 :

🪲 Still trying to figure out how I can post an event with connecting it to an existing creator uuid and event uuid.

**\~Solution~**
Still need to work on this!

### References Used Today:

https://jwtdown-fastapi.readthedocs.io/en/latest/intro.html

### Any Blockers:

I need to read more about uuids!

### Tomorrow I'm working on:

Fall break vacation, yay! & uuids

---

## 10-9-23

### Today I worked on:

- Coding the ‘Create an Event’ POST endpoint and committing it to remote. YAY!
- I will notify the team to review the code, approve, and merge it into main

**AH-HA!💡**
The creator uuid was giving me a hard time but I finally found out how to have the creator uuid automatically populate so that the user doesn’t have to input that info, when creating an event!

**🎉 Celebrations 🎉**
I’ve been trying to figure out how to do that for awhile, so I’m happy that I was able to resolve that blocker!

### Bugs encountered 🐛🐞🐜 :

🪲 The creator uuid was not automatically populating with user uuid. Then I figured out how to populate it but I was getting an error about the creator being a type uuid but the expression is type of numeric, so I had to convert it.

**\~Solution~**
I extracted the users uuid from the token, and then passed in the uuid as the creator in the repository.

### References Used Today:

https://jwtdown-fastapi.readthedocs.io/en/stable/intro.html
https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md
https://stackoverflow.com/questions/534839/how-to-create-a-guid-uuid-in-python
https://stackoverflow.com/questions/49663720/equivalent-to-pythons-uuid-uuid4-hex-in-javascript

### Any Blockers:

No more blockers at the moment.

### Tomorrow I'm working on:

Coding a GET endpoint for editing an Event.

### References for Tomorrow:

https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md

---

## 10-10-23

### Today I worked on:

- Completing a GET endpoint for getting a specific event!
- Testing, approving and merging MR from team members

**AH-HA!💡**
I was able to create my first GET endpoint. :)

**🎉 Celebrations 🎉**
I'm happy we are on track and moving at a good pace!

### Bugs encountered 🐛🐞🐜 :

🪲 Not bugs for the GET

**\~Solution~**

No solutions.

### References Used Today:

https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md

### Any Blockers:

No blockers at the moment.

### Tomorrow I'm working on:

Coding my PUT!

### References for Tomorrow:

Same reference, thanks to the Curtis!! He's the best!
https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md

---

## 10-11-12

### Today I worked on:

- Finishing my PUT endpoint for editing an event. I just labeled my MR as ready to merge, yay!
- I also tested, approved, and merge in my teammembers code.

**AH-HA!💡**
This was my first PUT. I found it a little tricky because I had to make sure that only the creator of the event was able to edit the event.

**🎉 Celebrations 🎉**
The team is still on track and we are working towards meeting our goals for the week.

### Bugs encountered 🐛🐞🐜 :

🪲 No bugs at the moment

### References Used Today:

https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md

### Any Blockers:

No blockers at this time, thankfully!

### Tomorrow I'm working on:

My GET all events endpoint!

## 10-12-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Pulling code down into my current PUT endpoint MR so that its up to date and my team members can approve/merge
- Started and finished my GET all events backend endpoint and asked a team member to review, approve, merge.
- Discussing with my team how we can improve our endpoints relating to user/business user and tea favorites.

**🎉 Celebrations 🎉**
We are working at a good pace, yay!

### References Used Today:

Good ol Curtis:
https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md

### Any Blockers:

None at the moment.

### Tomorrow I'm working on:

Starting on my DELETE endpoint which shouldn't take too long. Then, hopefully starting on a frontend component!

## 10/14/23

### Today I worked on:

- Starting on my DELETE endpoint but got hit with a blocker!
- Sharing my screen with the group to code the frontend authentication but we got stuck! Not sure if we are missing anything but we were getting hit with "Failed to get token after login" error. We will continue to work on this Monday.

### Bugs and Blockers encountered 🐛🐞🐜 :

🪲 "Failed to get token after login." error as mentioned above.
🪲 The DELETE endpoint behaves correctly when the current user is the creator, which is great! However, when the current user is not the creator, Swagger continues to return 'true' even though the action is not deleting the item in Beekeeper. I'm not sure why the FastAPI backend Swagger is still responding 'true' when the expected response should be 'false'.

### Tomorrow I'm working on:

- Finishing my delete endpoint
- Finishing the frontend authentication

## 10-15-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Completed 'Delete an event' backend endpoint! yay!
- Worked with team on CI/CD Backend and Frontend deployment and it's now good to go! WOWHOOO!
- Also completed the frontend authentication! So we have that up and running as well. So many things to celebrate!

### References Used Today:

- Rosheen's helpful notion on CI/CD!!!!!
- https://learn-2.galvanize.com/cohorts/3607/blocks/1908/content_files/build/01-ci-cd/01-continuous-integration.md
- https://learn-2.galvanize.com/cohorts/3733/blocks/1906/content_files/build/02-redux-frontend-auth/03-authorization-cookbook.md
- https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md

### Any Blockers:

None at this time!!!!!

### Tomorrow I'm working on:

FRONT END pages!!! So happy about that!

## 10-16-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Learned about Material UI!
- Modifying and completing the 'stylized' frontend pages for Sign up and Login. We might update these some more once we all decide on our projects color palette!
- Completing a 'Create an event' form page
- Starting on a 'View all events' page

### References Used Today:

- https://mui.com/

### Any Blockers:

No blockers at this time!

### Tomorrow I'm working on:

Continuing on with 'View all events' page

## 10-17-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Completing the View all events page!

### References Used Today:

- https://mui.com/

### Any Blockers:

No blockers at this time!

## 10-18-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Focused on deployment issues with the team. It works now, yay!
- Started on view a specific event page!

### References Used Today:

- https://mui.com/

### Any Blockers:

No blockers at this time!

## 10-19-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Hit a blocker with hiding 'edit event' button. I want it to only appear on the event details page if the current user uuid === event creator uuid.
- researching more about unit test since I have to start on that soon!

### References Used Today:

- https://mui.com/

## 10-23-23

### Today I worked on:

- Installing and learning about jwt-decode!
- Debugging with the group. We had some difficulty with the uuid but luckily Jordan was able to help us!! Now we can continue moving forward with our frontend without any uuid troubles.
- Due to the uuid modifications, I was able to finish my event details page and it's now ready to merge.

### References Used Today:

https://www.npmjs.com/package/jwt-decode

-

### Any Blockers:

None at this time!!!!!

### Tomorrow I'm working on:

Edit event form, delete event, and hopefully my unit test!

---

## 10-24-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Finished edit event form and also completed my unit test. It passed, wowhoo!!

**AH-HA!💡**
I learned about unit testing!

**🎉 Celebrations 🎉**
Everyone on the team got their unit tests to pass.

### References Used Today:

https://learn-2.galvanize.com/cohorts/3733/blocks/1906/content_files/build/04-unit-tests/04-unit-tests.md
Pauls lecture on unit test!

---

## 10-25-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Delete an event function. I added a 'delete event' button onto the event details page. This button is only visible if you are the creator of the event.
- I also had an issue with my vscode/gitlab authentication which Jordan helped me get out of! We recloned the repo with SSH instead of HTTPS. Then I was getting a lot of errors in my console but I didn't have them there before. It took awhile for me to find that the errors were due to the new repo missing our .env file. Thankfully it was an easy fix!

**🎉 Celebrations 🎉**
Relieved that Jordan was able to help me!!!!!

### References Used Today:

https://stackoverflow.com/questions/71775787/i-would-like-to-pop-up-confirmation-message-before-deleting-the-data-react-and-d

### Tomorrow I'm working on:

Documentation and README with the group!

## 10-26-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- Worked on the homepage frontend
- worked on the README documentation. I worked on the data models file. We will start with the actual README tomorrow as a group.
- found a small bug in the deployed frontend when deleting an event. It deletes but isnt removed from the page until I refresh. Working on this.

**🎉 Celebrations 🎉**
We're almost done!!!!

### References Used Today:

stackoverflow!! and HMU channel

### Tomorrow I'm working on:

README with the group and fixing that bug

## 10-26-23

### Today I worked on:

- Testing, approving and merging my team members code/MRs
- worked on the homepage frontend
- worked on front end edit form to have a box layout so the site is more consistent in theme/layout
- worked on the README documentation. I worked on the data models file. We will start with the actual README tomorrow as a group.
- found a small bug in the deployed frontend when deleting an event. It deletes but isnt removed from the page until I refresh. Working on this. I think I found a fix!

**🎉 Celebrations 🎉**
We're almost done!!!!

### References Used Today:

stackoverflow!! and HMU channel

### Tomorrow I'm working on:

README with the group
