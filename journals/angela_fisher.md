# Angela Fisher's Development Journal

---

## October 26, 2023

### Today I worked on:

 - Made a frontend component for the wiki gallery page
 - Added a 404 page to catch any incorrect url pathing
 - Lots of bug fixing for my components and the deployed site
 - Began work on documentation

**AH-HA!💡**

No major ah-has, but deployment highlighted some bugs we had overlooked so it felt good to catch those

**🎉 Celebrations 🎉**

The code freeze is in place: WE'RE DONE! Aside from bug fixes and documentation, but... it's a huge milestone!

### Bugs encountered 🐛🐞🐜 :

🪲 Some links on the deployed site went to 404s even though they were working on localhost.

**\~Solution~**

There were some hrefs that we'd overlooked -- links need to be handled through useNavigate!

🪲 I realized that when new teas are made on the deployed site, they disappear into the aether...

**\~Solution~**

It's not a full fix, because that would require another handful of components that I don't have time for (stretch goal!), but I added a switch on the tea form so that admins can choose to publish their newly submitted tea right away. That way, it will show up on the gallery page without issues.

### References Used Today:

No references today.

### Any Blockers:

No blockers!

### Tomorrow I'm working on:

 - Finishing up documentation
 - Presenting our project to the instructors!

---

## October 25, 2023

### Today I worked on:

 - Tidied up deployment so it works without issues now!
 - Updated the tea form component to support editing existing teas
 - Created the frontend component for displaying tea details. It's a doozy!

**AH-HA!💡**

Implementing the carousel on the tea detail page was a challenge, but it felt good to finally devise a working solution: split the array of nearby businesses into smaller arrays and map over businesses in groups of two. It looks much better and works great!

**🎉 Celebrations 🎉**

With the tea details page done, I've fully completed my frontend CRUD!

### Bugs encountered 🐛🐞🐜 :

🪲 API calls on the deployed site broke because the URL was being parsed incorrectly!

**\~Solution~**

It turns out GitLab stopped playing nice with variables. It was probably a temporary bug because it had been working in the past, but we couldn't take those risks and decided to hardcode our variables back into the gitlab-ci yml to ensure that the wonkiness can never occur again.

🪲 Found a bug with my business carousel on the tea detail page where if the number of businesses was odd, then the page would crash. Yikes!

**\~Solution~**

I had forgotten to account for the times when the arrays couldn't be perfect sets of two and had coded myself into a guaranteed off-by-one error. Very simple error with a super simple fix, but it was a good reminder that I need to slow down even in this final stretch :')

### References Used Today:

This is the library I used for the carousel: https://github.com/Learus/react-material-ui-carousel/blob/master/README.md
and I used a handful of StackOverflow threads to solve any problems that came up, but I forgot to save them

### Any Blockers:

No blockers!

### Tomorrow I'm working on:

 - Making my final (before grading) frontend component: the wiki gallery page.
 - Code freezing to work on the final debug and documentation

### References for Tomorrow:

None in mind

---

## October 24, 2023

### Today I worked on:

 - Completing my unit tests
 - Figuring out auth for my form
 - Finishing up the site navbar

**AH-HA!💡**

I was able to implement auth to the tea form and user-responsivity to the navbar by learning a lot more about useEffect and working around the limitations of the JWTdown library. I was able to implement useEffects to get all the user data I need in my components!

**🎉 Celebrations 🎉**

The team got unit tests completed today!! Another milestone down!

### Bugs encountered 🐛🐞🐜 :

Found another deployment bug... Gonna look into that tomorrow with the utmost urgency

### References Used Today:

Sorted out some more wonkiness with useEffect: https://dmitripavlutin.com/react-useeffect-infinite-loop/
and I used the unit test references I outlined yesterday :)

### Any Blockers:

Deployment might be a blocker. Made an HMU post so hoping for the best!

### Tomorrow I'm working on:

 - Fixing the deployment bug!!
 - Continuing work on my frontend CRUD. Aiming to have both the GET (tea details) and the PUT (tea update) done by EOD.

### References for Tomorrow:

No references in mind for tomorrow

---

## October 23, 2023

### Today (and over the weekend) I worked on:

 - Crafting a form with wondrous functionality :')
 - Wrestling with JWTdown for React

**AH-HA!💡**

We were able to fix a communication issue between our front and back ends that stemmed from JWTdown's lack of support for UUIDs. We had originally gotten around it by converting the UUIDs to ints, but that didn't translate well on the frontend. So, with Jordan's help, we were able to identify the correct changes to make to the backend to change UUIDs to strings in some select locations to better interact with the frontend.

**🎉 Celebrations 🎉**

The JWTdown/auth issues are all sorted!! Many, many hours were lost to troubleshooting everything that came up with that, but we're good to move forward now!

### Bugs encountered 🐛🐞🐜 :

Anything and everything under the sun involving JWTdown for React 😭 but solutions and workarounds were eventually found for all issues.

### References Used Today:

I used some tutorials to implement:
 - Regex URL validation: https://www.geeksforgeeks.org/check-if-an-url-is-valid-or-not-using-regular-expression/
 - Real-time form validation: https://www.youtube.com/watch?v=4YLa9iuN43c
I set up a couple new libraries:
 - React Toastify: https://fkhadra.github.io/react-toastify/introduction
 - JWT Decode: https://www.npmjs.com/package/jwt-decode

### Any Blockers:

No real blockers, but I wasn't able to fully complete my tea form today because I want to add authorization checks, too.

### Tomorrow I'm working on:

 - Adding my unit tests
 - More frontend! I want to finish the navbar ASAP because it affects all our other components since it takes up screen space.

### References for Tomorrow:

 - Paul's lecture on unit tests
 - https://learn-2.galvanize.com/cohorts/3733/blocks/1906/content_files/build/04-unit-tests/01-unit-tests.md

---

## October 19, 2023

### Today I worked on:

 - Finishing all the backend endpoints
 - Starting my work on frontend, officially!

**AH-HA!💡**

No major a-ha today.

**🎉 Celebrations 🎉**

Backend is DONE!!! At least, the MVP is. But I don't think we'll get the chance to do more :')

### Bugs encountered 🐛🐞🐜 :

Largely bugless today!

### References Used Today:

n/a

### Any Blockers:

No blockers!

### Tomorrow (aka this weekend) I'm working on:

 - Beginning work on my frontend components
 - Looking into unit testing

### References for Tomorrow:

 - https://fkhadra.github.io/react-toastify/introduction

---

## October 18, 2023

### Today I worked on:

 - Fixing deployment
 - More backend endpoints

**AH-HA!💡**

No major a-ha today.

**🎉 Celebrations 🎉**

Deployment is fixed!!

### Bugs encountered 🐛🐞🐜 :

🪲 Deployment was being suddenly finicky

**\~Solution~**

The problems were twofold: first, the cloud didn't have enough memory for all of the API servers so it crashed and they weren't working. However, once that was fixed we discovered that there are potentially errors if two services share the same name. We accidentally left our DB with the default name when we initially deployed it, so it stopped working as other people started getting their deployments configured and we had to redeploy it with a more unique name.
A quick note about glv-cloud-cli syntax: your service name cannot have capital letters in it, so make everything lowercase :)

### References Used Today:

 - One-on-one help from Rosheen! Thank you!!

### Any Blockers:

No blockers!

### Tomorrow I'm working on:

More backend because I really want it fully completed before we dive all the way into frontend.

### References for Tomorrow:

No references in mind.

---

## October 17, 2023

### Today I worked on:

 - More backend endpoints
 - Determining our CSS gameplan as a group
 - Setting up pre-commit
 - Attempting to diagnose an unexpected issue with deployment... 🥲

**AH-HA!💡**

Got the eslint hook for pre-commit working!! It just needed an extra argument to run correctly.

**🎉 Celebrations 🎉**

Lots of frontend done today -- Laura's a superstar!

### Bugs encountered 🐛🐞🐜 :

🪲 The pre-commit eslint hook would pass no matter what.

**\~Solution~**

Discovered, through the means of waaay too many stackoverflow threads and github issues, that eslint warnings won't fail the check. Eslint (or at least the version for React) ONLY throws warnings, never errors. So I had to add some args to make it acknoledge warnings: ```args: [--max-warnings=0]```

### References Used Today:

 - So many random internet threads that it wouldn't be worth listing them all ^^;

### Any Blockers:

Bit of a deployment blocker: while both the frontend and the backend are accessible individually, they don't seem to want to talk to each other right now. Uh oh!

### Tomorrow I'm working on:

Hoping to figure out this stinky deployment hiccup! Otherwise, I'll be continuing with backend and possibly frontend.

### References for Tomorrow:

🤷 I'm sure I'll need some but I have no clue lol

---

## October 16, 2023

### Today I worked on:

 - Backend endpoints continued
 - Deployment with the group!
 - Frontend auth with the group!

**AH-HA!💡**

I was able to determine that our frontend auth issue was simply bad data being sent to the API rather than a CORS issue, despite the errors not being clear about that.

**🎉 Celebrations 🎉**

We finished frontend auth today!! Huzzah!! Thanks to Abe and Jordan for helping us iron out some of the kinks in our jwtdown implementation.
We also got deployment done without a hitch in just a couple hours! That feels amazing!!

### Bugs encountered 🐛🐞🐜 :

🪲 We were getting some CORS errors when trying to send user data from the frontend.

**\~Solution~**

It turns out it wasn't a CORS error at all, I still have no idea why that was being thrown... But it was simply a bad request that required fixing some typos in our React component.

### References Used Today:

 - Lots of HMU!
 - Rosheen's amazingly detailed Notion for deployment (SO grateful!): https://complete-rutabaga-7b8.notion.site/Continuous-Integration-Continuous-Deployment-CI-CD-1016f1fb746f435b991bdb0d04601ff0
 - https://learn-2.galvanize.com/cohorts/3733/blocks/1908/content_files/build/01-ci-cd/01-continuous-integration.md

### Any Blockers:

No blockers now! We're in such a good spot!

### Tomorrow I'm working on:

Continuing with some backend endpoints so those can be completely checked off, then moving onto frontend (though probably not tomorrow lol)

### References for Tomorrow:

n/a

---

## October 13, 2023

### Today I worked on:

 - A complicated SQL query for the popular teas route
 - Beginning front-end auth with the group

**AH-HA!💡**

To make the SQL query that returns the data I needed in the format I needed, I had to learn Postgres window functions and some of its quirks with parameters. Understanding both of these were HUGE ah-ha moments for me!

**🎉 Celebrations 🎉**

Got the SQL query working the way I needed it to without too much trouble once I understood more Postgres. I learned that PostgreSQL works much differently than alternatives like MySQL and so I had to account for those differences when looking up troubleshooting help.

### Bugs encountered 🐛🐞🐜 :

🪲 The SQL query I originally wrote for popular teas was returning the data, but it was in such a format that the data was nearly impossible to extract consistently. The more teas I had in the database, the deeper nested the query results were in tuples. It started looking like ```[(((data,data,data),(data,data,data)))]```, which was NOT going to work lol.

### References Used Today:

 - This PostgreSQL window functions tutorial: https://www.youtube.com/watch?v=Ww71knvhQ-s
 - Subqueries from this StackOverflow thread: https://stackoverflow.com/questions/7606305/postgres-select-all-columns-but-group-by-one-column
 - Quirks of DISTINCT from this StackOverflow thread: https://stackoverflow.com/questions/12693089/pgerror-select-distinct-order-by-expressions-must-appear-in-select-list

### Any Blockers:

The SQL query was shaping up to be a blocker, but thankfully it's resolved!
As a group, we seem to have a blocker with frontend auth, where the wrong server is sent the login information. We aren't yet sure how to guarantee that the info is sent to localhost:8000.

### Tomorrow I'm working on:

Going to continue to look into frontend auth. Maybe Redux will be the solution!

### References for Tomorrow:

- https://learn-2.galvanize.com/cohorts/3733/blocks/1906/content_files/build/02-redux-frontend-auth/03-authorization-cookbook.md
- The JWTdown documentation for frontend
- https://learn-2.galvanize.com/cohorts/3733/blocks/1906/content_files/build/02-redux-frontend-auth/01-react-redux-tutorial.md

---

## October 12, 2023

### Today I worked on:

Deciding how to navigate some unexpected issues with our initial planning with the group. Also, more endpoints of course :)

**AH-HA!💡**

We realized that we should just call multiple endpoints in the frontend instead of writing out the same SQL queries in multiple endpoints. That way, the code is easier to write and maintain, and it's also DRY!

**🎉 Celebrations 🎉**

As a group, we're about halfway finished with all of our endpoints!

### Bugs encountered 🐛🐞🐜 :

No major bugs today!

### References Used Today:

No references used today.

### Any Blockers:

No blockers!

### Tomorrow I'm working on:

Continuing working on endpoints, primarily focusing on the favorites table.

### References for Tomorrow:

None (hopefully) needed.

---

## October 11, 2023

### Today I worked on:

Finishing up my first PUT endpoint. It's complete!

**AH-HA!💡**

No major ah-ha's today, but it took me a while to design the PUT endpoint for tea wiki page edits. I was pretty happy when I finally figured out a good way to accommodate all the different factors that need to be accounted for in the endpoint.

**🎉 Celebrations 🎉**

Having the PUT done is HUGE for me because I wasn't sure how to approach it going into it! So now I feel like I can comparatively zoom through the rest of my endpoints.

### Bugs encountered 🐛🐞🐜 :

No major bugs today!

### References Used Today:

Lots of W3 Schools to write my SQL queries correctly :')

### Any Blockers:

No blockers!

### Tomorrow I'm working on:

Mooooore endpoints.

### References for Tomorrow:

No particular references in mind for tomorrow's work.

---

## October 10, 2023

### Today I worked on:

More endpoints! I was working on my first PUT, but I still have post-break brain fog and needed to switch to one that was a little less involved. Instead I finished a GET.

**AH-HA!💡**

I was having a pesky bug where my GET method was erroring out when it tried to return the tea, but I figured out that I was returning the wrong thing -- pydantic is so picky!

**🎉 Celebrations 🎉**

Another API endpoint complete!! Just have to get through PUT and DELETE, then the rest of the backend will be a piece of cake.

### Bugs encountered 🐛🐞🐜 :

🪲 I was getting an "unprintable ValidationError object" error when I returned the row from the database, which was very unspecific and annoying!

**\~Solution~**

It was pydantic wanting the TeaOut object to be properly instantiated. I had to go through the dictionary returned from the SQL query and assign each property to a TeaOut object to satisfy this bug.


### References Used Today:

 - Nothing new today!

### Any Blockers:

No real blockers at this time :)

### Tomorrow I'm working on:

Continuing my work on the wiki tea pages' API endpoints.

### References for Tomorrow:

No particular references in mind for tomorrow's work.

---

## October 9, 2023

### Today (and over break) I worked on:

 - Updating our tables to have more consistency and functionality
 - Finished the first API endpoint: POST a new tea wiki page!
 - Starting the PUT endpoint for tea wiki pages
 - Fixed the small bug that caused our fastapi docker container to crash on initial startup

**AH-HA!💡**

I scoured the jwtdown documentation to understand how to require authentication for an endpoint and access the logged-in user's data. It took some time to understand it fully!

**🎉 Celebrations 🎉**

The first API endpoint is complete! Things are really getting moving now!

### Bugs encountered 🐛🐞🐜 :

🪲 On creation of a new tea, the "published" status should be optional to pass in because it's supposed to default to false if unspecified; however, when it was left out of the POST request, an error was thrown.

**\~Solution~**

Even though the published column has a default in the database, it isn't possible to cleanly make it optional when crafting queries to the database via FastAPI. Instead, I was able to use Pydantic (I think?) to set a default value to the optional variable so that it would not be null when a database query was made, regardless of whether or not it was passed through in the API call.


### References Used Today:

 - For user_data from authentication: https://jwtdown-fastapi.readthedocs.io/en/stable/api-reference.html#jwtdown_fastapi.authentication.Authenticator.try_get_current_account_data
 - For table modifications: https://www.postgresql.org/docs/10/sql-createtable.html and https://www.postgresql.org/docs/current/tutorial-fk.html
 - For resolving the fastapi container crash: https://github.com/ufoscout/docker-compose-wait

### Any Blockers:

No real blockers at this time :)

### Tomorrow I'm working on:

Continuing my work on the wiki tea pages' API endpoints.

### References for Tomorrow:

No particular references in mind for tomorrow's work.

---

## September 28, 2023

### Today I worked on:

 - Ironing out the authentication bugs with the group
 - Beginning work on API endpoints: I started with POSTing a new tea page!

**AH-HA!💡**

We were able to successfully identify and fix the root of our errors with authentication. It turns out we didn't have a good grasp of the authentication methods, so we had been looking for solutions in the wrong spot.

**🎉 Celebrations 🎉**

Authentication is fully set up!! Now we can get started on the backend of our project!

### Bugs encountered 🐛🐞🐜 :

🪲 The /users POST route that was supposed to create a new user and log them in with authentication was returning a 500 response instead of the newly created user.

**\~Solution~**

We discovered that while the user was being saved to the database, there was an issue retrieving the data for that user to send back through the API. The problem was that the authentication method for grabbing the user data required the username of the user, when we had been passing the UUID. It turned out to be a very simple fix that had just been causing a lot of headaches!


### References Used Today:

The video on authentication in Learn, and the official documentation for jwtdown:
https://jwtdown-fastapi.readthedocs.io/en/stable/api-reference.html

### Any Blockers:

No more blockers! Yay!

### Tomorrow I'm working on:

Over the break I intend to finish up the routes for teas that I started working on today.

### References for Tomorrow:

I'll definitely need to look into jwtdown some more so that I can properly secure the tea creation endpoint. See this link: https://jwtdown-fastapi.readthedocs.io/en/stable/_modules/jwtdown_fastapi/authentication.html#Authenticator.get_account_data

---

## September 27, 2023

### Today I worked on:

 - Creating our SQL tables in migrations
 - Beginning work on authentication using jwtdown

**AH-HA!💡**

I have a better understanding of how SQL migrations work now!

**🎉 Celebrations 🎉**

Our group successfully set up our database complete with its tables! Huzzah!!

### Bugs encountered 🐛🐞🐜 :

🪲 We were running into issues getting the create_user function in our authentication router to complete successfully.

**\~Solution~**

We don't have a solution yet... It's a blocker right now 😢


### References Used Today:

The video on authentication in Learn, and the official documentation for jwtdown:
https://jwtdown-fastapi.readthedocs.io/en/stable/intro.html

### Any Blockers:

We're unsure how to proceed to resolve the errors with our authentication right now.

### Tomorrow I'm working on:

We will continue to mob-program authentication! Hopefully we can figure it out quickly so that we can break off into working on individual endpoints.

### References for Tomorrow:

Same references as today :)

---

## September 26, 2023

### Today I worked on:

 - Learning how to do merge requests properly
 - Adding a PostgreSQL database to our project
 - Setting up our project to get started on the code tomorrow!

**AH-HA!💡**

I gained more insight into how Docker compose files are structured and formatted. Nice!

**🎉 Celebrations 🎉**

Our group made our first successful merge request! The project is ours now!

### Bugs encountered 🐛🐞🐜 :

🪲 A team member was unable to pull from main after my MR was approved and joined with the main branch.

**\~Solution~**

They had uncommitted changes on their local main branch and had to handle those changes before they could pull the remote changes.
Note to future readers: do NOT handle these changes by committing them to main and attempting to resolve the resulting merge conflict. It's not a great idea. We spent a lot of time undoing the damage from that choice... 😅

### References Used Today:

The code committed today was done with guidance from Learn!

### Any Blockers:

The issue with being unable to pull from main after the MR was a massive blocker, but it was resolved and the team learned a valuable lesson!

### Tomorrow I'm working on:

We will be mob-programming authentication!

### References for Tomorrow:

The explore about authentication in Learn will be invaluable! If we get stuck, we'll have to revisit Paul's lecture about authentication from today.

---

Big thanks to SEIR Jordan for the journal template!
