### 10-27-23

### Today we worked on: Today, we focused on documentation and we're prepping for presentations. We are happy with our work, but the only thing we're really bummed out about is the styling of our website. We are planning to get that done next week for our stretch goals.

### No bugs encountered! 🐛🐞🐜

### 10-26-23

### Today we worked on: We all continued working on our frontend component till 2:00 PM. We decided to code freeze and start testing everything. We had a few bugs but we are working with it. We got most of it fixed but there are still a lot of styling that needs to be done. I managed to finish a get all reviews for a specific business that will be useful for my frontend component. For the rest of the night I will be working on the documentations and getting everything ready for tomorrow.

### No bugs encountered! 🐛🐞🐜

### 10-25-23

### Today we worked on: Today, we continued working on our frontend component. I completed a frontend component that allows any users to post a review for a specific business. I thought about wanting to style my frontend component but it feels too empty at the moment. I plan on creating an API Endpoint which will GET all business review, so that way I can have a list of reviews on a specific business users account. The next week for stretch goals, I plan on making my frontend look more pretty for presentation.

### No bugs encountered! 🐛🐞🐜

### 10-24-23

### Today we worked on: We all started on our unit test today and it went smooth! Everyone was able to complete at least 1 unit test, it took me many tries but I eventually got get all location unit test to work. Decided to continue on frontend component since I only got 1 done so far, I'd want to contribute more. Tomorrow I'm hoping to get my ReviewForm.js done.

### No bugs encountered! 🐛🐞🐜

### 🎉 Celebrations 🎉: Was not expecting  to finish my unit test by the end of the day but I managed. I was stuck at first trying to figure out how to do a unit test for all business users. I asked my partners for some feedback and they suggested to try working on getting all location first, and it worked out perfectly. For stretch goals I'm going to try to create a unit test for all business users again.

### 10-23-23

### Today we worked on: I officially completed my first frontend component with the help of my partners! The issue was that I needed to create a .env file to get it working. As soon as I created .env and transfer the code from .env.sample it worked. After that we decided to all go back to users.py to fixed the uuid. It took us awhile, so we asked Jordan, our SIER to help us out. We eventually got it working and we had to change a few codes in our file. I personally had to go through all of my old files and fix a few codes. We tested it, and it all worked out. With the uuid being officially fixed, our workflow should be more smooth now.

### No bugs encountered! 🐛🐞🐜

### 10-19-23

### Today we worked on: We decided to focus on review for today, as soon as that was done we went back to frontend components, we all decided to research more on unit testing for this week on our break. Hopefully when we come back, we'll have a better understanding of how to implement it.

### 🎉 Celebrations 🎉: It has been a long stressful week, but I'm glad I got a lot of experience working on this project with my group. I will be rewarding myself this week by relaxing but also studying the review a little.

### No bugs encountered! 🐛🐞🐜

### 10-18-23

### Today we worked on: My group all continued to work on on their own Frontend Component. I was barely finishing up my last API Endpoint. I officially started on my own Frontend Component today, it's very confusing so far since I haven't gotten used to it yet. I researched documentations today and watched some videos on it. I attempted to apply UseEffect and UseState, imported a few stuff, but it's getting late so I'll continue tomorrow. On the bright side, I believe our group is doing really well at the moment, we are getting things done quite quickly. My goal is to get at least 2 frontend component done before the end of Monday.

### AH-HA!💡: Felt pretty accomplished when I realized the reason why I wasn't able to get what I wanted from my database for my API endpoint was because of how I was selecting it. It took me a while, but once I spotted it. Felt really relief to get it working.

### 🎉 Celebrations 🎉: After finally catching up on the Frontend Component, felt pretty accomplished that I was able to debug some of the problems I had earlier on my own before approaching my group for help. I will be sleeping in later tonight as a reward for myself c:


### No bugs encountered! 🐛🐞🐜


### 10-17-23

### Today we worked on: Today, I finished up two API Endpoints which were "PUT/DELETE Business Review". I'm am almost done with my last API Endponit which is "GET Business Near You", hopefully by tomorrow I'll be working on the frontend componenents. I am planning to revisit my API Endpoints later on when I'm satisfied with the frontend componenent, there are are a few ideas that I had in mind. For now, my goal is to reach the checkpoint for week 16; which is looking good so far.

### No bugs encountered! 🐛🐞🐜

### 10-16-23

### Today we worked on: Successful day, we got our deployment figured out. My group were able to help me debug my problem with POST Business Review, decided to clean it up a little. Moving onto delete business review tomorrow and hopefully frontend.

### No bugs encountered! 🐛🐞🐜

### 10-13-23

### Today we worked on: We all continued to work on our API endpoint, I continue with my POST Business Review; currently stuck. I'll be asking for some help from my group on monday. We realized that we also needed to setup a frontend authenticator so we all worked as a group to get that situated, we were getting his weird bug that we couldn't figure out, we went at it for an hour and decided to take a break. We will try and finish it on monday when we come back.

### bugs encountered! 🐛🐞🐜 Couldn't fetch the token, there was another group that had the same issue, hopefully can figure it out on monday.

### 10-12-23

### Today we worked on: Feeling pretty good, everyone worked on their API endpoints, I continue working on GET business profile and I was able to finish it getting help from HMU. I was also able to finish up another API endpoint which was POST business profile. Tomorrow I will be continueing on my POST Business Review API endpoint tomorrow.

### No bugs encountered! 🐛🐞🐜

### 10-11-23

### Today we worked on: API Endpoint, I was able to get my GET Business Profile to work but it wasn't sending out our data, I kept getting null. I'm attempting to work on it again tomorrow, if I'm still stuck I plan on asking my group to help me out, and if that doesn't work out then I'll try using HMU.

### Bugs encountered 🐛🐞🐜: Having issues not getting data back from our database. But I am still getting a 200 response.

### 10-10-23

### Today we worked on:I started on the get busines profile, I was tweaking our POST Business Review API endpoint for a bit, I wanted to clean it up a little before starting on the GET Business Profile. I believe I should be able to finish it by the end of the day tomorrow and see if POST Business review will work or not.

### No bugs encountered! 🐛🐞🐜

### 10-09-23
### I think I am officially done with POST a new Business Review, but to test it out I need to create a business first. This should've been done before the review. Tomorrow I will be working on creating a Business and I'm going to test to see if it works.

### No bugs encountered! 🐛🐞🐜



### 9-27-23
### Today we worked on: We successfully created an authenticator for the backend. It took us a bit to get it working but we eventually successfully got it working. Our goal now is to each select an issue to work with, I've decided to go with the business review endpoints. Starting with POST.

### No Bugs encountered! 🐛🐞🐜
### 9-27-23

### Today we worked on: We successfully created a database which went smoothly, at first we were going to stick with pgadmin but decided to stick with beekeeper. Personally I think beekeeper looks a lot more clean compare to pgadmin so I'm glad we made the switch. After creating our database we attempted to work on authentication endpoints. This was extremely tedious since everythings new to us, we went back and forth watching vids and comparing our codes, we got pretty far and it feels like we're close to completing;however, we've been dealing with an error for almost an hour. We decided to step away from coding for today and to rest up for tomorrow and see if we can figure it out later.

### Bugs encountered 🐛🐞🐜:  We kept getting an error that mentioned something was wrong within our router? We've tried messing with the uuid and googling the solution and comparing but still can't figure out what's the issue.

**\~Solution~**

### References Used Today:

### FastAPI video https://learn-2.galvanize.com/cohorts/3733/blocks/1893/content_files/build/02-rest-fastapi/10-fast-api-videos.md
### Backend Authenticator https://learn-2.galvanize.com/cohorts/3733/blocks/1897/content_files/build/02-backend-auth/01-using-jwtdown-fastapi.md
### Repository that we used to compare https://gitlab.com/sjp19-public-resources/fastapi-and-postgresql
### Documents https://jwtdown-fastapi.readthedocs.io/en/stable/intro.html

### 9-26-23

### Today I worked on: We were trying to get the project set up today by setting up postgresql, we've decided to use pgAdmin; however, I was thinking about maybe switching to beekeeper. For standup meeting tomorrow, I planned to discussed about it with my group to see if they'd like to switch to beekeeper or stick to pgadmin.

### Bugs encountered 🐛🐞🐜: not a bug perse but an issue with figuring out how to handle merge request, I wasn't able to pull from the main.

**\~Solution~**

### Resolved: The issue was that while I was practicing on my own PC, I forgot to create my own branch before merging, this caused an issue on gitlab end. We had to revert some changes and got it fixed.
