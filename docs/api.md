# APIs

## Users

- **Method**: `POST`, `GET`, `DELETE`,
- **Path**: `/users`, `/users/<uuid>`,

Input:

```json
{
  "username": string,
  "role": string,
  "img_url": string,
  "display_name": int,
  "description": int,
  "hours": int,
  "location": int,
  "password": string
}
```

Output:

```json
{
  "username": string,
  "role": string,
  "img_url": string,
  "display_name": int,
  "description": int,
  "hours": int,
  "location": int,
  "uuid": UUID4
}
```

Creating a new user saves their desired username, password, profile picture, location, and if they are a business: their business description and operating hours. The role determines the user's type: 0 for regular (the default), 1 for business, and 2 for admin.

## Teas

- **Method**: `GET`, `GET`, `GET`, `PUT`, `PUT`, `DELETE`,
- **Path**: `/teas`, `/teas/<uuid>`, `/teas/popular`, `/teas/<uuid>/publish`

Input:

```json
{
  "name": string,
  "description": string,
  "img_url": string,
  "published": bool
}
```

Output:

```json
{
  "name": string,
  "description": string,
  "img_url": string,
  "published": bool,
  "uuid": UUID4
}
```

Creating a new tea saves its name, description, image URL, and whether or not it is published to the public. All get requests, aside from individual UUIDs, only return published teas. Published is set to False by default, but an admin is able to change it to true either during tea creation or by calling the publish endpoint path. Calling popular teas returns the top five most-liked teas in the database.

## Events

- Method: `GET`, `GET`, `GET`, `POST`, `PUT`, `DELETE`
- Path: `/events`, `/events/<uuid>`, `/events/nearby`

Input:

```json
{
  "title": string,
  "location": string,
  "date": date,
  "time": string,
  "description": string,
  "img_url": string
}
```

Output:

```json
{
  "title": string,
  "location": string,
  "date": date,
  "time": string,
  "description": string,
  "img_url": string,
  "uuid": string,
  "creator": string
}
```

Creating an event takes all of the inputs and saves them alongside the creator's UUID, so that they are able to have control over that data if they wish to edit or remove it.

## Businesses

- Method: `GET`, `GET`, `PUT`
- Path: `/business/<UUID>`, `/business/location/<LOCATION>`

Input:

```json
{
  "username": string,
  "role": int,
  "img_url": string,
  "display_name": string,
  "description": string,
  "hours": string,
  "location": string,
  "password": string
}
```

Output:

```json
{
  "uuid": string,
  "username": string,
  "role": int,
  "img_url": string,
  "display_name": string,
  "description": string,
  "hours": string,
  "location": string
}
```

The business API is tied to the users table in the database, but it specifically caters to users with the business role (1). Businesses can be filtered by location to display only those in a certain area.

## Reviews

- Method: `GET`, `POST`, `PUT`, `DELETE`
- Path: `/reviews/<BUSINESS UUID>`, `/reviews/<REVIEW UUID>`

Input:

```json
{
  "rating": int,
  "comment": string
}
```

Output:

```json
{
  "rating": int,
  "comment": string,
  "uuid": UUID4,
  "business": UUID4,
  "author": UUID4
}
```

Reviews are dependant on businesses, as a review cannot exist without a corresponding business. Once created, reviews store the author UUID so that they can edit or delete that review if needed. Reviews are fetched in batches based on the corresponding business's UUID.

## Favorites

- Method: `GET`, `POST`, `DELETE`
- Path: `/favorites`, `/favorites/<UUID>`, `/users/<UUID>/favorites`

Input:

```json
{
  "tea_uuid": UUID4,
  "user_uuid": UUID4
}
```

Output:

```json
{
  "tea_uuid": UUID4,
  "user_uuid": UUID4,
  "uuid": UUID4
}
```

The favorites table in the database is an associative table where users and teas are connected. Adding a favorite, then, requires both a user and a tea UUID. Sending a GET request grabs the favorite in batches tied to a user's UUID.
