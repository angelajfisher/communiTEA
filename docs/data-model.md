# Data models

## CommuniTEA

---

### Users

| name         | type               | unique | optional |
| ------------ | ------------------ | ------ | -------- |
| uuid         | uuid - primary key | yes    | no       |
| username     | string             | yes    | no       |
| role         | integer            | no     | no       |
| img_url      | string             | no     | yes      |
| display_name | string             | yes    | no       |
| description  | string             | no     | yes      |
| hours        | string             | no     | yes      |
| location     | string             | no     | yes      |

### Teas

| name        | type               | unique | optional |
| ----------- | ------------------ | ------ | -------- |
| uuid        | uuid - primary key | yes    | no       |
| name        | string             | yes    | no       |
| description | string             | no     | no       |
| img_url     | string             | no     | yes      |
| published   | bool               | no     | no       |

## Drafts

| name        | type                  | unique | optional |
| ----------- | --------------------- | ------ | -------- |
| uuid        | uuid                  | yes    | no       |
| tea_uuid    | reference to tea uuid | no     | no       |
| img_url     | string                | no     | yes      |
| description | string                | no     | yes      |

### Favorites

| name      | type                   | unique | optional |
| --------- | ---------------------- | ------ | -------- |
| uuid      | uuid - primary key     | yes    | no       |
| user_uuid | reference to user uuid | yes    | no       |
| tea_uuid  | reference to tea uuid  | yes    | no       |

### Reviews

| name     | type                   | unique | optional |
| -------- | ---------------------- | ------ | -------- |
| uuid     | uuid - primary key     | yes    | no       |
| rating   | integer                | no     | no       |
| comment  | string                 | no     | yes      |
| author   | reference to user uuid | yes    | no       |
| business | reference to user uuid | yes    | no       |

### Events

| name     | type                   | unique | optional |
| -------- | ---------------------- | ------ | -------- |
| uuid     | uuid - primary key     | yes    | no       |
| creator  | reference to user uuid | yes    | no       |
| title    | string                 | no     | no       |
| location | string                 | yes    | no       |
| date     | date                   | no     | no       |
| time     | time                   | no     | no       |
| img_url  | string                 | no     | yes      |
