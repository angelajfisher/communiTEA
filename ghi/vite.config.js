import { TanStackRouterVite } from "@tanstack/router-vite-plugin";
import react from "@vitejs/plugin-react-swc";
import { defineConfig, loadEnv } from "vite";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
	const env = loadEnv(mode, process.cwd(), "");

	return {
		plugins: [TanStackRouterVite(), react()],
		server: {
			host: true,
			port: 3000,
			strictPort: true,
			watch: {
				usePolling: true,
			},
		},
		base: env.VITE_BASE,
	};
});
