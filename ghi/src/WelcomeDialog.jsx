import {
	Box,
	Button,
	Checkbox,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	useMediaQuery,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { useNavigate } from "@tanstack/react-router";
import { useState } from "react";
import wave from "./../public/wave.svg";

export const WelcomeDialog = () => {
	const navigate = useNavigate();
	const theme = useTheme();
	const [open, setOpen] = useState(true);
	const [rememberMe, setRememberMe] = useState(false);
	const fullScreen = useMediaQuery(theme.breakpoints.down("xs"));

	const handleClose = () => {
		localStorage.setItem("noDemo", rememberMe);
		setOpen(false);
	};

	return (
		<Dialog
			fullScreen={fullScreen}
			open={open}
			onClose={handleClose}
			aria-labelledby="responsive-dialog-title"
		>
			<DialogTitle id="responsive-dialog-title">
				{"Hey there"} <img src={wave} alt="waving hand" height="25rem" />{" "}
				{"Welcome to CommuniTEA!"}
			</DialogTitle>
			<DialogContent>
				<DialogContentText>
					While you can enjoy much of the site&apos;s functionality without logging
					in, to get the most out of this demo it&apos;s recommended that you create
					a new business user account with the location &quot;Seattle, WA&quot;.
				</DialogContentText>
			</DialogContent>
			<DialogActions style={{ justifyContent: "space-between" }}>
				<Box>
					<Checkbox
						size="small"
						value={rememberMe}
						onClick={(e) => {
							setRememberMe(!rememberMe);
						}}
					/>{" "}
					Don&apos;t show this again
				</Box>
				<Box>
					<Button
						autoFocus
						onClick={(e) => {
							navigate({ to: "/signup", search: { demo: true } });
							handleClose();
						}}
					>
						Take me there
					</Button>
					<Button onClick={handleClose}>Close</Button>
				</Box>
			</DialogActions>
		</Dialog>
	);
};
