import { RouterProvider, createRouter } from "@tanstack/react-router";
import { useContext } from "react";
import { routeTree } from "./routeTree.gen";
import { AuthContext } from "./utils";

const router = createRouter({
	routeTree,
	basepath: import.meta.env.VITE_BASE,
	context: { authState: { authenticated: false } },
});

function App() {
	const [authState] = useContext(AuthContext);
	return <RouterProvider router={router} context={{ authState }} />;
}

export default App;
