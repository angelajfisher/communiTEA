import { Box, Button, Paper, Typography } from "@mui/material";
import { Link } from "@tanstack/react-router";

export default function PageNotFound() {
	return (
		<Paper elevation={4} sx={{ p: 2, m: 2, mt: 7 }}>
			<Box sx={{ bgcolor: "background.paper", py: 6 }}>
				<Typography
					component="h1"
					variant="h3"
					align="left"
					color="text.primary"
				>
					404
				</Typography>
				<Typography
					gutterBottom
					variant="h6"
					align="left"
					color="text.secondary"
				>
					That page doesn&apos;t exist!
				</Typography>
				<Link to="/">
					<Button variant="contained">Return to Home</Button>
				</Link>
			</Box>
		</Paper>
	);
}
