import { CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { ToastContainer } from "react-toastify";
import App from "./App";
import { AuthContextProvider } from "./utils";
import "react-toastify/dist/ReactToastify.css";
import "./index.css";

const theme = createTheme({
	// Create a custom theme at https://zenoo.github.io/mui-theme-creator/
	palette: {
		mode: "light",
		primary: {
			main: "#365939",
			contrastText: "#ffffff",
		},
		secondary: {
			main: "#a08769",
		},
		background: {
			default: "#f9f8eb",
			paper: "#f3efe4",
		},
		error: {
			main: "#b22b29",
		},
		info: {
			main: "#3893d0",
		},
		success: {
			main: "#35943a",
		},
	},
	typography: {
		h1: {
			fontFamily: "Roboto",
		},
	},
});

const rootElement = document.getElementById("root");
if (!rootElement.innerHTML) {
	const root = createRoot(rootElement);
	root.render(
		<StrictMode>
			<ThemeProvider theme={theme}>
				<AuthContextProvider>
					<App />
				</AuthContextProvider>
				<CssBaseline />
				<ToastContainer
					position="bottom-right"
					autoClose={5000}
					hideProgressBar
					newestOnTop
					closeOnClick
					rtl={false}
					pauseOnFocusLoss
					draggable
					pauseOnHover
					stacked
					theme="colored"
				/>
			</ThemeProvider>
		</StrictMode>,
	);
}
