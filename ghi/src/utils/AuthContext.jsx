import { createContext, useCallback, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { unauthenticated } from "./unauthenticated";

const AuthContext = createContext([{}, () => {}]);

const AuthContextProvider = ({ children }) => {
	const [authState, setAuthState] = useState({
		...unauthenticated,
		firstLoad: true,
	});

	const checkAuthStatus = useCallback(async () => {
		const response = await fetch(
			`${import.meta.env.VITE_API_HOST}/authenticate/`,
			{ credentials: "include" },
		);
		if (response.ok) {
			const userData = await response.json();
			if (userData != null) {
				setAuthState({ authenticated: true, ...userData });
			} else if (!authState.firstLoad) {
				toast.warning("You have been signed out.");
				setAuthState(unauthenticated);
			}
		}
	}, [authState.firstLoad]);

	useEffect(() => {
		checkAuthStatus();
	}, [checkAuthStatus]);

	useEffect(() => {
		if (authState.authenticated) {
			const interval = setInterval(() => {
				checkAuthStatus();
			}, 1800000);

			return () => clearInterval(interval);
		}
	}, [authState.authenticated, checkAuthStatus]);

	return (
		<AuthContext.Provider value={[authState, setAuthState]}>
			{children}
		</AuthContext.Provider>
	);
};

export { AuthContext, AuthContextProvider };
