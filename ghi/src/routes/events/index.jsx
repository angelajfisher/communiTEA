import {
	Box,
	Button,
	Card,
	CardActions,
	CardContent,
	CardMedia,
	Container,
	CssBaseline,
	FormControl,
	Grid,
	InputLabel,
	MenuItem,
	Select,
	Stack,
	Typography,
} from "@mui/material";
import { Link, createFileRoute } from "@tanstack/react-router";
import { AuthContext } from "../../utils";
import "./Events.css";
import { useContext, useEffect, useState } from "react";

export const Route = createFileRoute("/events/")({
	component: EventsList,
});

function EventsList() {
	const [authState] = useContext(AuthContext);
	const [events, setEvents] = useState([]);
	const [filteredEvents, setFilteredEvents] = useState(events);
	const [filterLocation, setFilterLocation] = useState(authState.location);
	const [userLocations, setUserLocations] = useState([]);

	useEffect(() => {
		const getData = async () => {
			const response = await fetch(`${import.meta.env.VITE_API_HOST}/events/`);
			if (response.ok) {
				const data = await response.json();
				setEvents(data);

				const locations = new Set(data.map((event) => event.location));
				locations.delete(authState.location);
				setUserLocations([...locations]);
			}
		};
		getData();
		setFilterLocation(authState.location);
	}, [authState.location]);

	useEffect(() => {
		setFilteredEvents(
			events.filter(
				(event) => !filterLocation || event.location === filterLocation,
			),
		);
	}, [filterLocation, events]);

	function formatTime(timeString) {
		const date = new Date(`1970-01-01T${timeString}Z`);

		const options = {
			hour: "numeric",
			minute: "2-digit",
			hour12: true,
			timeZone: "UTC",
		};

		const formattedTime = date.toLocaleTimeString("en-US", options);

		return formattedTime;
	}
	events.sort((a, b) => new Date(a.date) - new Date(b.date));

	return (
		<Box sx={{ backgroundColor: "#fafaf1", minHeight: "100vh" }}>
			<CssBaseline />
			<main>
				<Box
					sx={{
						pt: 17,
						pb: 3,
						mt: 4,
						backgroundColor: "#fafaf1",
					}}
				>
					<Container maxWidth="lg">
						<Typography
							component="h1"
							variant="h3"
							align="center"
							color="text.primary"
							gutterBottom
							className="drop-down"
						>
							Upcoming Events
						</Typography>
						<Typography
							style={{ fontSize: "19px" }}
							align="center"
							color="text.primary"
							className="drop-down"
						>
							Get involved with the CommuniTea! Share your passion for tea,
							connect with like-minded enthusiasts, and learn more by attending
							or hosting your own events.
						</Typography>
					</Container>
				</Box>
				<Container sx={{ py: 0, backgroundColor: "#fafaf1" }} maxWidth="xl">
					<Box display="flex" justifyContent="center" marginBottom={0}>
						<FormControl variant="outlined" sx={{ mb: 3, width: 1 / 2 }}>
							<InputLabel>Filter by Location</InputLabel>
							<Select
								value={filterLocation}
								onChange={(e) => setFilterLocation(e.target.value)}
								label="Filter by Location"
							>
								<MenuItem value="">None</MenuItem>
								{authState.location !== "" && (
									<MenuItem value={authState.location}>
										{authState.location}
									</MenuItem>
								)}
								{userLocations.map((location) => (
									<MenuItem key={location} value={location}>
										{location}
									</MenuItem>
								))}
							</Select>
						</FormControl>
					</Box>
					<Grid container spacing={3} justifyContent="center">
						{filteredEvents.length > 0 ? (
							filteredEvents.map((event) => (
								<Grid item key={event.uuid} xs={12} sm={6} md={4}>
									<Link
										to={`/events/${event.uuid}`}
										style={{ textDecoration: "none" }}
									>
										<Card
											className="cardHover"
											sx={{
												height: "100%",
												display: "flex",
												flexDirection: "column",
											}}
										>
											<CardMedia
												component="img"
												sx={{
													width: "100%",
													height: "300px",
													objectFit: "cover",
												}}
												image={
													event.img_url ||
													"https://images.pexels.com/photos/6913382/pexels-photo-6913382.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
												}
												alt={event.title}
											/>
											<CardContent
												align="center"
												sx={{
													flexGrow: 1,
													backgroundColor: "#FFFEFC",
												}}
											>
												<Typography
													marginTop={3}
													gutterBottom
													variant="h5"
													component="h2"
												>
													{event.title}
												</Typography>
												<Typography
													variant="body2"
													color="text.primary"
													sx={{ marginTop: 2 }}
												>
													<b>Location:</b> {event.location}
												</Typography>
												<Typography variant="body2" color="text.primary">
													<b>Date:</b>
													{new Date(event.date).toLocaleDateString()}
												</Typography>
												<Typography variant="body2" color="text.primary">
													<b>Time:</b> {formatTime(event.time)}
												</Typography>
											</CardContent>
											<CardActions
												sx={{
													justifyContent: "flex-end",
													backgroundColor: "#FFFEFC",
												}}
											>
												<Button size="small">View</Button>
											</CardActions>
										</Card>
									</Link>
								</Grid>
							))
						) : (
							<Box width="50%" textAlign="center" pt={8}>
								<Typography variant="header1">
									No upcoming events could be found.
								</Typography>
								<Typography variant="body2">
									Try filtering to a different location or get the party started
									by hosting your own!
								</Typography>
							</Box>
						)}
					</Grid>
				</Container>

				<Stack
					sx={{ pt: 8 }}
					direction="row"
					spacing={2}
					justifyContent="center"
				>
					<Link to="/events/create">
						<Button variant="contained">Host your own event</Button>
					</Link>
				</Stack>
			</main>
			<Box sx={{ p: 10 }} component="footer">
				<Typography variant="h6" align="center" gutterBottom />
				<Typography
					variant="subtitle2"
					align="center"
					color="text.secondary"
					component="p"
				/>
			</Box>
		</Box>
	);
}
