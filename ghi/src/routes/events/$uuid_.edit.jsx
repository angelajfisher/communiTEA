import { createFileRoute, redirect, useParams } from "@tanstack/react-router";
import EventForm from "./-EventForm";

export const Route = createFileRoute("/events/$uuid/edit")({
  component: EventEditForm,
  beforeLoad: ({ context, location }) => {
    if (!context.authState.authenticated) {
      throw redirect({
        to: "/login",
        search: {
          redirect: location.href,
        },
      });
    }
  },
});

function EventEditForm() {
  const params = useParams({ from: "/events/$uuid/edit" });
  return EventForm(params.uuid);
}
