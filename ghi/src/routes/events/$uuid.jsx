import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	Grid,
	Typography,
} from "@mui/material";
import { createFileRoute } from "@tanstack/react-router";
import { Link, useNavigate, useParams } from "@tanstack/react-router";
import { useCallback, useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext } from "../../utils";
import "./Events.css";

export const Route = createFileRoute("/events/$uuid")({
	component: EventDetails,
});

function EventDetails() {
	const [authState] = useContext(AuthContext);
	const [event, setEvent] = useState({});
	const params = useParams({ from: "/events/$uuid" });
	const navigate = useNavigate({ from: Route.fullPath });
	const [delConfirmation, setDeleteConfirmation] = useState(false);

	const getData = useCallback(async () => {
		const response = await fetch(
			`${import.meta.env.VITE_API_HOST}/events/${params.uuid}/`,
		);
		if (response.ok) {
			const data = await response.json();
			setEvent(data);
		}
	}, [params.uuid]);

	useEffect(() => {
		getData();
	}, [getData]);

	function formatTime(timeString) {
		const date = new Date(`1970-01-01T${timeString}Z`);

		const options = {
			hour: "numeric",
			minute: "2-digit",
			hour12: true,
			timeZone: "UTC",
		};

		const formattedTime = date.toLocaleTimeString("en-US", options);

		return formattedTime;
	}

	async function deleteEvent(uuid) {
		const response = await fetch(
			`${import.meta.env.VITE_API_HOST}/events/${uuid}/`,
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json",
				},
				credentials: "include",
			},
		);

		if (response.ok) {
			toast.success("Event successfully deleted!");
			navigate({ to: "/events" });
		}
	}

	return (
		<Grid
			container
			component="main"
			sx={{ height: "100vh", backgroundColor: "#fbfcf7" }}
		>
			<Grid
				item
				xs={false}
				sm={4}
				md={5.5}
				sx={{
					backgroundImage: `url(${
						event.img_url ||
						"https://images.pexels.com/photos/6913382/pexels-photo-6913382.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
					})`,
					backgroundRepeat: "no-repeat",
					backgroundColor: (t) =>
						t.palette.mode === "light"
							? t.palette.grey[50]
							: t.palette.grey[900],
					backgroundSize: "cover",
					backgroundPosition: "center",
				}}
			/>
			<Grid
				item
				xs={12}
				sm={8}
				md={5}
				component={Box}
				elevation={6}
				sx={{
					p: 10,
					transition: "transform 1s ease-in-out",
					animation: "slideTopToBottom 1s ease-in-out",
					backgroundColor: "#fbfcf7",
				}}
				className="slide-top-to-bottom"
			>
				<Box
					sx={{
						my: 15,
						mx: 4,
						display: "flex",
						flexDirection: "column",
						alignItems: "flex-start",
						backgroundColor: "#fbfcf7",
					}}
				>
					<Typography
						component="h1"
						variant="h4"
						gutterBottom
						sx={{ lineHeight: 1.5, mb: 3 }}
					>
						{event.title}
					</Typography>
					<Typography
						variant="body1"
						gutterBottom
						sx={{ lineHeight: 2.5, mb: 6 }}
					>
						{event.description}
					</Typography>
					<Typography variant="body1" gutterBottom sx={{ lineHeight: 1.2 }}>
						<b>Location:</b> {event.location}
					</Typography>
					<Typography variant="body1" gutterBottom sx={{ lineHeight: 1.2 }}>
						<b>Date:</b> {new Date(event.date).toLocaleDateString()}
					</Typography>
					<Typography
						variant="body1"
						color="text.primary"
						gutterBottom
						sx={{ lineHeight: 1.2 }}
					>
						<b>Time:</b> {formatTime(event.time)}
					</Typography>

					<Box sx={{ mt: 15 }}>
						{authState.uuid === event.creator && (
							<>
								<Link to={`/events/${event.uuid}/edit/`}>
									<Button sx={{ mr: 3 }}>Edit event</Button>
								</Link>
								<Button
									className="btn custom-delete-button"
									sx={{ mr: 3 }}
									onClick={() => setDeleteConfirmation(true)}
								>
									DELETE EVENT
								</Button>
								<Dialog
									open={delConfirmation}
									onClose={() => setDeleteConfirmation(false)}
								>
									<DialogTitle
										sx={{
											fontSize: "18px",
											backgroundColor: "#fbfcf7",
										}}
									>
										Are you sure you want to delete your event?
									</DialogTitle>

									<DialogActions sx={{ backgroundColor: "#fbfcf7" }}>
										<Button
											onClick={() => setDeleteConfirmation(false)}
											color="primary"
										>
											Cancel
										</Button>
										<Button
											onClick={() => {
												setDeleteConfirmation(false);
												deleteEvent(event.uuid);
											}}
											color="primary"
										>
											Delete
										</Button>
									</DialogActions>
								</Dialog>
							</>
						)}
						<Link to="/events">
							<Button>See more events</Button>
						</Link>
					</Box>
				</Box>
			</Grid>
		</Grid>
	);
}
