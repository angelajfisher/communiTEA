import { createFileRoute, redirect } from "@tanstack/react-router";
import EventForm from "./-EventForm";

export const Route = createFileRoute("/events/create")({
  component: EventForm,
  beforeLoad: ({ context, location }) => {
    if (!context.authState.authenticated) {
      throw redirect({
        to: "/login",
        search: {
          redirect: location.href,
        },
      });
    }
  },
});
