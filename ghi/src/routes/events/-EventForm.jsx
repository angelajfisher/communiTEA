import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Container,
  Grid,
  IconButton,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import { Link, useNavigate } from "@tanstack/react-router";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "./../../utils";

export default function EventForm(uuid) {
  const [authState, setAuthState] = useContext(AuthContext);
  const [formData, setFormData] = useState({
    title: "",
    location: authState.location,
    date: "",
    time: "",
    description: "",
    img_url: "",
  });
  const [focused, setFocused] = useState([]);
  const [errors, setErrors] = useState({});
  const [submitDisabled, setSubmitDisabled] = useState(true);
  const navigate = useNavigate();

  const validateInput = () => {
    const temp = { ...errors };
    const required = "This field is required.";

    temp.title = formData.title ? "" : required;
    temp.date = formData.date ? "" : required;
    temp.time = formData.time ? "" : required;
    temp.description = formData.description ? "" : required;

    if (formData.location !== "") {
      temp.location = /[A-Z][a-zA-Z]+,[ ]?[A-Z]{2}/gm.test(formData.location)
        ? ""
        : "Please enter a valid city and state abbreviation. Field is case-sensitive.";
    }

    if (formData.img_url === "") {
      temp.img_url = "";
    } else {
      temp.img_url =
        /^(http(s?):\/\/.)[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/g.test(
          formData.img_url,
        )
          ? ""
          : "Please enter a valid URL.";
    }

    if (JSON.stringify(errors) !== JSON.stringify(temp)) {
      setErrors({ ...temp });
      setSubmitDisabled(!Object.values(temp).every((x) => x === ""));
    }
  };

  const handleFocus = (e) => {
    e.preventDefault();
    const inputName = e.target.name;

    if (!focused.includes(inputName)) {
      setFocused([...focused, inputName]);
    }
  };

  const handleFormChange = (e) => {
    e.preventDefault();
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleFormSubmission = async (e) => {
    e.preventDefault();
    const temp = { ...formData };
    if (temp.img_url === "") {
      temp.img_url = null;
    }

    let url;
    let fetchConfig;

    if (typeof uuid === "string") {
      url = `${import.meta.env.VITE_API_HOST}/events/${uuid}`;
      fetchConfig = {
        method: "put",
        body: JSON.stringify(temp),
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
      };
    } else {
      url = `${import.meta.env.VITE_API_HOST}/events/`;
      fetchConfig = {
        method: "post",
        body: JSON.stringify(temp),
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
      };
    }

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      if (typeof uuid === "string") {
        toast.success("Event successfully updated!");
        navigate({ to: "/events/$uuid", params: { uuid: uuid } });
      } else {
        toast.success("Event successfully created!");
        setFormData({
          title: "",
          location: "",
          date: "",
          time: "",
          description: "",
          img_url: "",
        });
        setFocused([]);
      }
    } else {
      const detail = await response.json();
      toast.error(`An error occurred. ${detail.message}`);
      if (response.status === 401) {
        setAuthState(unauthenticated);
      }
    }
  };

  useEffect(() => {
    const getEvent = async () => {
      if (typeof uuid === "string") {
        const response = await fetch(
          `${import.meta.env.VITE_API_HOST}/events/${uuid}/`,
        );
        if (response.ok) {
          const data = await response.json();
          if (!data.img_url) {
            data.img_url = "";
          }
          setFormData({ ...data });
        }
      }
    };
    getEvent();
  }, [uuid]);

  useEffect(() => {
    validateInput();
  });

  return (
    <Container component="main" maxWidth="xs">
      <Paper
        sx={{
          marginTop: 15,
          marginBottom: 5,
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-end",
        }}
        elevation={8}
      >
        <IconButton
          component={Link}
          to="/events"
          aria-label="return to my communitea"
        >
          <CloseIcon />
        </IconButton>
        <Box
          sx={{
            paddingTop: 0,
            paddingX: 3,
            paddingBottom: 4,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            {typeof uuid === "string"
              ? `Edit ${formData.name}`
              : "Create a New Event"}
          </Typography>
          <Box component="form" onSubmit={handleFormSubmission} sx={{ mt: 3 }}>
            <Grid container spacing={2} display="flex" alignItems="center">
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handleFormChange}
                  onFocus={handleFocus}
                  name="title"
                  id="title"
                  label="Event Title"
                  value={formData.title}
                  error={
                    focused.includes("title") ? Boolean(errors.title) : false
                  }
                  helperText={focused.includes("title") ? errors.title : false}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handleFormChange}
                  onFocus={handleFocus}
                  name="location"
                  id="location"
                  label="City, State Abbv."
                  value={formData.location}
                  error={
                    focused.includes("location")
                      ? Boolean(errors.location)
                      : false
                  }
                  helperText={
                    focused.includes("location") ? errors.location : false
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  InputLabelProps={{ shrink: true }}
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handleFormChange}
                  onFocus={handleFocus}
                  name="date"
                  type="date"
                  id="date"
                  label="Date"
                  value={formData.date}
                  error={
                    focused.includes("date") ? Boolean(errors.date) : false
                  }
                  helperText={focused.includes("date") ? errors.date : false}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  InputLabelProps={{ shrink: true }}
                  variant="outlined"
                  required
                  fullWidth
                  onChange={handleFormChange}
                  onFocus={handleFocus}
                  name="time"
                  type="time"
                  id="time"
                  label="Time"
                  value={formData.time}
                  error={
                    focused.includes("time") ? Boolean(errors.time) : false
                  }
                  helperText={focused.includes("time") ? errors.time : false}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  multiline
                  minRows={4}
                  required
                  fullWidth
                  onChange={handleFormChange}
                  onFocus={handleFocus}
                  name="description"
                  id="description"
                  label="Event Description"
                  value={formData.description}
                  error={
                    focused.includes("description")
                      ? Boolean(errors.description)
                      : false
                  }
                  helperText={
                    focused.includes("description") ? errors.description : false
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  onFocus={handleFocus}
                  onChange={handleFormChange}
                  name="img_url"
                  id="img_url"
                  label="Image URL"
                  value={formData.img_url}
                  error={Boolean(errors.img_url)}
                  helperText={errors.img_url}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              disabled={Boolean(submitDisabled)}
              sx={{ mt: 3 }}
            >
              Submit
            </Button>
            {submitDisabled && (
              <Typography component="p" variant="caption" align="center">
                Please satisfy all form requirements.
              </Typography>
            )}
          </Box>
        </Box>
      </Paper>
    </Container>
  );
}
