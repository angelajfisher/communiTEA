import MenuIcon from "@mui/icons-material/Menu";
import {
	AppBar,
	Avatar,
	Box,
	Button,
	Container,
	IconButton,
	Menu,
	MenuItem,
	Toolbar,
	Tooltip,
	Typography,
} from "@mui/material";
import { Link, useNavigate } from "@tanstack/react-router";
import { useContext, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "../utils";

export default function Navbar() {
	const [authState, setAuthState] = useContext(AuthContext);
	const navigate = useNavigate();

	const pages = { wiki: "WikiTEAdia", events: "My Communitea" };
	const userlessActions = { signup: "Sign Up", login: "Log In" };

	const [anchorElNav, setAnchorElNav] = useState(null);
	const [anchorElUser, setAnchorElUser] = useState(null);

	const handleOpenNavMenu = (event) => {
		setAnchorElNav(event.currentTarget);
	};
	const handleOpenUserMenu = (event) => {
		setAnchorElUser(event.currentTarget);
	};

	const handleCloseNavMenu = (page) => {
		page && navigate({ to: `/${page}` });
		setAnchorElNav(null);
	};

	const handleCloseUserMenu = () => {
		setAnchorElUser(null);
	};

	const handleLogout = async (e) => {
		const response = await fetch(`${import.meta.env.VITE_API_HOST}/signout/`, {
			method: "delete",
			credentials: "include",
		});
		if (response.ok) {
			toast.success("Successfully logged out. See you later!");
		}
		setAuthState(unauthenticated);
		navigate({ to: "/" });
	};

	return (
		<AppBar position="fixed">
			<Container maxWidth="xl">
				<Toolbar disableGutters>
					{/* Desktop view */}
					<Typography
						variant="h6"
						noWrap
						component="p"
						sx={{
							mr: 2,
							display: { xs: "none", md: "flex" },
							fontWeight: 700,
							color: "inherit",
							textDecoration: "none",
						}}
					>
						<Link style={{ textDecoration: "none", color: "white" }} to="/">
							CommuniTEA
						</Link>
					</Typography>

					<Box
						sx={{
							flexGrow: 1,
							display: { xs: "none", md: "flex" },
						}}
					>
						{Object.keys(pages).map((page) => (
							<Button
								key={page}
								onClick={(e) => handleCloseNavMenu(page)}
								sx={{ my: 2, color: "white", display: "block" }}
							>
								{pages[page]}
							</Button>
						))}
					</Box>

					{/* Mobile view */}
					<Box
						sx={{
							flexGrow: 1,
							display: { xs: "flex", md: "none" },
						}}
					>
						<IconButton
							size="medium"
							aria-label="account of current user"
							aria-controls="menu-appbar"
							aria-haspopup="true"
							onClick={handleOpenNavMenu}
							color="inherit"
						>
							<MenuIcon />
						</IconButton>
						<Menu
							id="menu-appbar"
							anchorEl={anchorElNav}
							anchorOrigin={{
								vertical: "bottom",
								horizontal: "left",
							}}
							keepMounted
							transformOrigin={{
								vertical: "top",
								horizontal: "left",
							}}
							open={Boolean(anchorElNav)}
							onClose={(e) => handleCloseNavMenu()}
							sx={{
								display: { xs: "block", md: "none" },
							}}
						>
							{Object.keys(pages).map((page) => (
								<MenuItem key={page} onClick={(e) => handleCloseNavMenu(page)}>
									<Typography textAlign="center">{pages[page]}</Typography>
								</MenuItem>
							))}
						</Menu>
					</Box>
					<Typography
						variant="h5"
						noWrap
						component="p"
						sx={{
							mr: 2,
							display: { xs: "flex", md: "none" },
							flexGrow: 1,
							fontWeight: 700,
							color: "inherit",
							textDecoration: "none",
						}}
					>
						<Link style={{ textDecoration: "none", color: "white" }} to="/">
							CommuniTEA
						</Link>
					</Typography>

					{/* Both desktop and mobile views */}
					<Box sx={{ flexGrow: 0 }}>
						{/* Logged-in user view */}
						{authState.authenticated && (
							<>
								<Tooltip title="Open settings">
									<IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
										<Avatar alt="profile image" src={authState.img_url} />
									</IconButton>
								</Tooltip>
								<Menu
									sx={{ mt: "45px" }}
									id="menu-appbar"
									anchorEl={anchorElUser}
									anchorOrigin={{
										vertical: "top",
										horizontal: "right",
									}}
									keepMounted
									transformOrigin={{
										vertical: "top",
										horizontal: "right",
									}}
									open={Boolean(anchorElUser)}
									onClose={handleCloseUserMenu}
								>
									<MenuItem
										onClick={(e) => {
											navigate({
												to: "/users/$uuid",
												params: { uuid: authState.uuid },
											});
											handleCloseUserMenu();
										}}
									>
										<Typography textAlign="center">
											<Button>My Profile</Button>
										</Typography>
									</MenuItem>
									<MenuItem
										onClick={(e) => {
											handleLogout();
											handleCloseUserMenu();
										}}
									>
										<Typography textAlign="center">
											<Button>Logout</Button>
										</Typography>
									</MenuItem>
								</Menu>
							</>
						)}

						{/* Logged-out user view */}
						{!authState.authenticated && (
							<>
								{/* Desktop */}
								<Box
									sx={{
										flexGrow: 1,
										display: { xs: "none", md: "flex" },
									}}
								>
									{Object.keys(userlessActions).map((action) => (
										<Typography
											key={action}
											sx={{
												mx: 1,
												my: 2,
												color: "white",
												display: "block",
											}}
										>
											<Link
												style={{
													textDecoration: "none",
													color: "white",
												}}
												to={`/${action}`}
											>
												{userlessActions[action]}
											</Link>
										</Typography>
									))}
								</Box>
								{/* Mobile */}
								<Box sx={{ display: { xs: "flex", md: "none" } }}>
									<Link
										style={{
											textDecoration: "none",
											color: "white",
										}}
										to={"/login"}
									>
										Login
									</Link>
								</Box>
								<Menu
									sx={{ mt: "45px" }}
									id="menu-appbar"
									anchorEl={anchorElUser}
									anchorOrigin={{
										vertical: "top",
										horizontal: "right",
									}}
									keepMounted
									transformOrigin={{
										vertical: "top",
										horizontal: "right",
									}}
									open={Boolean(anchorElUser)}
									onClose={handleCloseUserMenu}
								>
									{Object.keys(userlessActions).map((action) => (
										<MenuItem key={action} onClick={handleCloseUserMenu}>
											<Typography textAlign="center">
												<Link
													style={{
														textDecoration: "none",
														color: "black",
													}}
													to={`/${action}`}
												>
													{userlessActions[action]}
												</Link>
											</Typography>
										</MenuItem>
									))}
								</Menu>
							</>
						)}
					</Box>
				</Toolbar>
			</Container>
		</AppBar>
	);
}
