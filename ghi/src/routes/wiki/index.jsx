import SearchIcon from "@mui/icons-material/Search";
import {
	Box,
	Button,
	Card,
	CardActionArea,
	CardActions,
	CardContent,
	CardMedia,
	Container,
	Grid,
	InputAdornment,
	Paper,
	Stack,
	TextField,
	Typography,
} from "@mui/material";
import { Link, createFileRoute, useNavigate } from "@tanstack/react-router";
import { useContext, useEffect, useState } from "react";
import Carousel from "react-material-ui-carousel";
import { AuthContext } from "../../utils";

export const Route = createFileRoute("/wiki/")({
	component: WikiGallery,
});

function WikiGallery() {
	const [authState] = useContext(AuthContext);
	const [teas, setTeas] = useState([]);
	const [popTeas, setPopTeas] = useState([]);
	const [searchQuery, setSearchQuery] = useState("");
	const navigate = useNavigate({ from: Route.fullPath });
	const placeholderImg =
		"https://images.pexels.com/photos/2659387/pexels-photo-2659387.jpeg";

	useEffect(() => {
		const getTeas = async () => {
			const response = await fetch(`${import.meta.env.VITE_API_HOST}/teas/`);
			if (response.ok) {
				const data = await response.json();
				setTeas(data);
			}
		};

		const getPopTeas = async () => {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/popular/`,
			);
			if (response.ok) {
				const data = await response.json();
				setPopTeas(data);
			}
		};

		getTeas();
		getPopTeas();
	}, []);

	return (
		<Paper elevation={4} sx={{ p: 2, m: 2, mt: 7 }}>
			<Box sx={{ bgcolor: "background.paper", py: 6 }}>
				<Container maxWidth="lg">
					<Stack direction="row">
						<Stack mx={4} sx={{ width: 3 / 4 }}>
							<Typography
								component="h1"
								variant="h3"
								align="left"
								color="text.primary"
							>
								WikiTEAdia
							</Typography>
							<Typography
								gutterBottom
								variant="h6"
								align="left"
								color="text.secondary"
							>
								Learn more about your favorite teas and discover something new!
							</Typography>
							{authState.role >= 1 && (
								<Stack
									sx={{ pt: 10 }}
									direction="row"
									spacing={2}
									justifyContent="left"
								>
									<Link to="/wiki/teas/create">
										<Button variant="contained">
											{authState.role === 2
												? "Submit a New Tea"
												: "Suggest a New Tea"}
										</Button>
									</Link>
									{authState.role === 2 && (
										<Link to="/wiki/manage">
											<Button variant="contained">Manage Wiki</Button>
										</Link>
									)}
								</Stack>
							)}
						</Stack>
						<Box display="flex" justifyContent="center" sx={{ width: 1 / 4 }}>
							<Stack sx={{ width: 1 }}>
								<Typography sx={{ width: 1 }}>Most Popular Teas</Typography>
								<Carousel>
									{popTeas.map((tea) => (
										<Card
											key={tea.name}
											sx={{
												height: "100%",
												display: "flex",
												flexDirection: "column",
											}}
										>
											<CardActionArea
												onClick={(e) =>
													navigate({
														to: "/wiki/teas/$uuid",
														params: { uuid: tea.uuid },
													})
												}
											>
												<CardMedia
													component="img"
													sx={{
														width: "100%",
														height: "150px",
														objectFit: "cover",
													}}
													image={tea.img_url || placeholderImg}
													alt={tea.name}
												/>
												<CardContent>
													<Typography
														align="center"
														gutterBottom
														variant="h6"
														component="div"
													>
														{tea.name}
													</Typography>
												</CardContent>
											</CardActionArea>
										</Card>
									))}
								</Carousel>
							</Stack>
						</Box>
					</Stack>
				</Container>
			</Box>
			<Container sx={{ py: 0, mb: 4 }} maxWidth="lg">
				<Paper elevation={4} sx={{ p: 5 }}>
					<Box display="flex" justifyContent="center" sx={{ width: 1 }}>
						<TextField
							onChange={(e) => setSearchQuery(e.target.value)}
							label="Search all teas"
							sx={{ mb: 5, width: 1 / 2 }}
							InputProps={{
								startAdornment: (
									<InputAdornment position="start">
										<SearchIcon />
									</InputAdornment>
								),
							}}
							variant="standard"
						/>
					</Box>
					<Grid container spacing={4}>
						{teas
							.filter(({ name }) =>
								name.toLowerCase().includes(searchQuery.toLowerCase()),
							)
							.map((tea) => (
								<Grid item key={tea.uuid} xs={12} sm={6} md={4}>
									<Card
										sx={{
											height: "100%",
											display: "flex",
											flexDirection: "column",
										}}
									>
										<CardMedia
											component="img"
											sx={{
												width: "100%",
												height: "250px",
												objectFit: "cover",
											}}
											image={tea.img_url || placeholderImg}
											alt={tea.name}
										/>
										<CardContent sx={{ flexGrow: 1 }}>
											<Typography gutterBottom variant="h5" component="h2">
												{tea.name}
											</Typography>
											<Typography noWrap>{tea.description}</Typography>
										</CardContent>
										<CardActions>
											<Link to="/wiki/teas/$uuid" params={{ uuid: tea.uuid }}>
												<Button size="small">View</Button>
											</Link>
										</CardActions>
									</Card>
								</Grid>
							))}
					</Grid>
				</Paper>
			</Container>
		</Paper>
	);
}
