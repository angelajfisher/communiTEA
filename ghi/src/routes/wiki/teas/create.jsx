import { createFileRoute, redirect } from "@tanstack/react-router";
import TeaForm from "./-TeaForm";

export const Route = createFileRoute("/wiki/teas/create")({
	component: TeaForm,
	beforeLoad: ({ context, location }) => {
		if (!context.authState.authenticated) {
			throw redirect({
				to: "/login",
				search: {
					redirect: location.href,
				},
			});
		}
	},
});
