import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import FavoriteOutlinedIcon from "@mui/icons-material/FavoriteOutlined";
import {
	Box,
	Button,
	Card,
	CardActionArea,
	CardContent,
	CardMedia,
	Dialog,
	DialogActions,
	DialogTitle,
	Grid,
	IconButton,
	Paper,
	Stack,
	Tooltip,
	Typography,
} from "@mui/material";
import {
	Link,
	createFileRoute,
	useNavigate,
	useParams,
} from "@tanstack/react-router";
import { useCallback, useContext, useEffect, useState } from "react";
import Carousel from "react-material-ui-carousel";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "../../../utils";

export const Route = createFileRoute("/wiki/teas/$uuid")({
	component: TeaDetail,
});

function TeaDetail() {
	const [authState, setAuthState] = useContext(AuthContext);
	const [tea, setTea] = useState({});
	const [favTea, setFavTea] = useState(null);
	const [nearbyBis, setNearbyBis] = useState([]);
	const [delDialogStatus, setDelDialogStatus] = useState(false);
	const [pubDialogStatus, setPubDialogStatus] = useState(false);
	const params = useParams({ from: "/wiki/teas/$uuid" });
	const navigate = useNavigate({ from: Route.fullPath });

	const handleRemoveFav = async (e) => {
		e.preventDefault();
		const response = await fetch(
			`${import.meta.env.VITE_API_HOST}/favorites/${favTea}/`,
			{
				method: "delete",
				headers: {
					"Content-Type": "application/json",
				},
				credentials: "include",
			},
		);
		if (response.ok) {
			toast.success(`${tea.name} was successfully removed from your list.`);
			setFavTea(null);
			getFavTea();
		} else {
			toast.error("Could not remove tea.");
		}
	};

	const handleAddFav = async (e) => {
		e.preventDefault();
		if (!authState.authenticated) {
			toast.error("You must be logged in to continue.");
		} else {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/favorites/`,
				{
					method: "post",
					body: `{"tea_uuid": "${params.uuid}", "user_uuid": "${authState.uuid}"}`,
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				},
			);
			if (response.ok) {
				toast.success(`${tea.name} was successfully added to your list.`);
				const data = await response.json();
				setFavTea(data);
				getFavTea();
			} else {
				const detail = await response.json();
				toast.error(`An error occurred. ${detail.message}`);
				if (response.status === 401) {
					setAuthState(unauthenticated);
				}
			}
		}
	};

	const handleDelete = async () => {
		if (authState.role === 2) {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/${params.uuid}/`,
				{
					method: "delete",
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				},
			);
			if (response.ok) {
				toast.success("Tea successfully deleted.");
				navigate({ to: "/wiki" });
			} else {
				toast.error("Could not delete tea.");
				setDelDialogStatus(false);
			}
		} else {
			toast.error("You are not authorized to delete.");
		}
	};

	const handlePublish = async () => {
		if (authState.role === 2) {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/${params.uuid}/publish/`,
				{
					method: "put",
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				},
			);
			if (response.ok) {
				toast.success("Tea successfully published.");
				navigate({ to: "/wiki/teas/$uuid", params: { uuid: params.uuid } });
			} else {
				const detail = await response.json();
				toast.error(`An error occurred. ${detail.message}`);
				if (response.status === 401) {
					setAuthState(unauthenticated);
				}
			}
			setPubDialogStatus(false);
		} else {
			toast.error("You are not authorized to publish.");
		}
	};

	const getFavTea = useCallback(async () => {
		if (authState.uuid) {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/users/${authState.uuid}/favorites/`,
			);
			if (response.ok) {
				const data = await response.json();
				const fav = data.filter((fav) => fav.tea_uuid === params.uuid)[0];

				if (fav?.tea_uuid === params.uuid) {
					setFavTea(fav.uuid);
				} else {
					setFavTea(null);
				}
			}
		} else {
			setFavTea(null);
		}
	}, [authState.uuid, params.uuid]);

	const getBusinesses = useCallback(async () => {
		const response = await fetch(
			`${import.meta.env.VITE_API_HOST}/teas/${params.uuid}/businesses/`,
			{
				method: "get",
				headers: {
					"Content-Type": "application/json",
				},
				credentials: "include",
			},
		);
		if (response.ok) {
			const data = await response.json();
			const bisGroups = [];

			while (data.length > 0) {
				bisGroups.push(data.splice(0, 2));
			}
			setNearbyBis(bisGroups);
		}
	}, [params.uuid]);

	useEffect(() => {
		const getTeaDetails = async () => {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/${params.uuid}/`,
			);
			if (response.ok) {
				const data = await response.json();
				setTea(data);
			}
		};
		getTeaDetails();
	}, [params.uuid]);

	useEffect(() => {
		getFavTea();
		if (authState.authenticated) {
			getBusinesses();
		}
	}, [authState.authenticated, getFavTea, getBusinesses]);

	return (
		<Grid container component="main" sx={{ height: "100vh" }}>
			<Grid
				item
				xs={false}
				sm={4}
				md={5.5}
				sx={{
					backgroundImage: `url(${tea.img_url})`,
					backgroundRepeat: "no-repeat",
					backgroundColor: (t) =>
						t.palette.mode === "light"
							? t.palette.grey[50]
							: t.palette.grey[900],
					backgroundSize: "cover",
					backgroundPosition: "center",
				}}
			/>
			<Grid
				item
				xs={12}
				sm={8}
				md={5}
				component={Box}
				elevation={6}
				sx={{ py: 10, paddingLeft: 10 }}
			>
				<Box
					sx={{
						my: 7,
						mx: 4,
						display: "flex",
						flexDirection: "column",
						alignItems: "flex-start",
					}}
				>
					<Stack direction="row" justifyContent="flex-end" sx={{ width: 1 }}>
						{tea.published ? (
							favTea ? (
								<Tooltip title="Remove from Favorites">
									<IconButton
										p={0}
										aria-label="Remove tea from favorites"
										onClick={handleRemoveFav}
									>
										<FavoriteOutlinedIcon sx={{ fontSize: 30 }} />
									</IconButton>
								</Tooltip>
							) : (
								<Tooltip title="Add to Favorites">
									<IconButton
										aria-label="Add tea to favorites"
										onClick={handleAddFav}
									>
										<FavoriteBorderOutlinedIcon sx={{ fontSize: 30 }} />
									</IconButton>
								</Tooltip>
							)
						) : (
							<>
								<Button
									onClick={(e) => setPubDialogStatus(true)}
									size="small"
									sx={{ color: "error", ml: 2 }}
								>
									Publish Tea
								</Button>
								<Dialog
									open={pubDialogStatus}
									onClose={(e) => setPubDialogStatus(false)}
									aria-labelledby="alert-dialog-title"
									aria-describedby="alert-dialog-description"
								>
									<DialogTitle id="alert-dialog-title">
										{`Publish ${tea.name}? This will make it visible to the public!`}
									</DialogTitle>
									<DialogActions>
										<Button onClick={handlePublish}>Yes, Publish.</Button>
										<Button
											autoFocus
											onClick={(e) => setPubDialogStatus(false)}
											sx={{ color: "error" }}
										>
											Cancel
										</Button>
									</DialogActions>
								</Dialog>
							</>
						)}
					</Stack>
					<Typography
						component="h1"
						variant="h4"
						gutterBottom
						sx={{ lineHeight: 1.5, mb: 3 }}
					>
						{tea.name}
					</Typography>
					<Typography
						variant="body1"
						gutterBottom
						sx={{ lineHeight: 2.5, mb: 6 }}
					>
						{tea.description}
					</Typography>
					{authState.authenticated && (
						<Paper elevation={2} sx={{ p: 3, width: 8 / 9 }}>
							<Typography
								component="h6"
								align="center"
								gutterBottom
								sx={{ fontSize: 20, mb: 4 }}
							>
								Businesses Near You Serving {tea.name}
							</Typography>
							{nearbyBis.length > 0 ? (
								<Carousel navButtonsAlwaysInvisible>
									{nearbyBis.map((bisGroup) => (
										<Stack
											key={bisGroup[0].uuid + bisGroup[1]?.uuid}
											direction="row"
											sx={{ width: 1 }}
										>
											<Card key={bisGroup[0].name} sx={{ width: 1 / 2 }}>
												<CardActionArea
													onClick={(e) =>
														navigate({
															to: "/users/$uuid",
															params: { uuid: bisGroup[0].uuid },
														})
													}
												>
													<CardMedia
														component="img"
														height="100"
														image={
															bisGroup[0].img_url ||
															"https://cwdaust.com.au/wpress/wp-content/uploads/2015/04/placeholder-store.png"
														}
														alt={bisGroup[0].name}
													/>
													<CardContent>
														<Typography
															align="center"
															gutterBottom
															variant="h6"
															component="div"
														>
															{bisGroup[0].name}
														</Typography>
													</CardContent>
												</CardActionArea>
											</Card>
											{bisGroup[1] && (
												<Card key={bisGroup[1].name} sx={{ width: 1 / 2 }}>
													<CardActionArea
														onClick={(e) =>
															navigate({
																to: "/users/$uuid",
																params: { uuid: bisGroup[1].uuid },
															})
														}
													>
														<CardMedia
															component="img"
															height="100"
															image={
																bisGroup[1].img_url ||
																"https://cwdaust.com.au/wpress/wp-content/uploads/2015/04/placeholder-store.png"
															}
															alt={bisGroup[1].name}
														/>
														<CardContent>
															<Typography
																align="center"
																gutterBottom
																variant="h6"
																component="div"
															>
																{bisGroup[1].name}
															</Typography>
														</CardContent>
													</CardActionArea>
												</Card>
											)}
										</Stack>
									))}
								</Carousel>
							) : (
								<Typography align="center" variant="subtitle1" component="p">
									There are no businesses near you serving this tea.
								</Typography>
							)}
						</Paper>
					)}
					<Box sx={{ mt: 5 }}>
						<Link to="/wiki">
							<Button size="small">Return to Wiki</Button>
						</Link>
						{authState.authenticated && authState.role !== 0 && (
							<Link to="/wiki/teas/$uuid/edit" params={{ uuid: params.uuid }}>
								<Button size="small" sx={{ ml: 2 }}>
									{authState.role === 2
										? "Edit Tea Details"
										: "Suggest a Change"}
								</Button>
							</Link>
						)}
						{authState.authenticated && authState.role === 2 && (
							<>
								<Button
									onClick={(e) => setDelDialogStatus(true)}
									size="small"
									sx={{ color: "error", ml: 2 }}
								>
									Delete Tea
								</Button>
								<Dialog
									open={delDialogStatus}
									onClose={(e) => setDelDialogStatus(false)}
									aria-labelledby="alert-dialog-title"
									aria-describedby="alert-dialog-description"
								>
									<DialogTitle id="alert-dialog-title">
										{`Delete ${tea.name}? This action is irreversible!`}
									</DialogTitle>
									<DialogActions>
										<Button onClick={handleDelete}>Yes, Delete.</Button>
										<Button
											autoFocus
											onClick={(e) => setDelDialogStatus(false)}
											sx={{ color: "error" }}
										>
											Cancel
										</Button>
									</DialogActions>
								</Dialog>
							</>
						)}
					</Box>
				</Box>
			</Grid>
		</Grid>
	);
}
