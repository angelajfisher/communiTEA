import CloseIcon from "@mui/icons-material/Close";
import {
	Box,
	Button,
	Container,
	Grid,
	IconButton,
	Paper,
	Switch,
	TextField,
	Typography,
} from "@mui/material";
import { Link, useNavigate } from "@tanstack/react-router";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "../../../utils";

export default function TeaForm(uuid) {
	const [authState, setAuthState] = useContext(AuthContext);
	const [formData, setFormData] = useState({
		name: "",
		description: "",
		img_url: "",
		published: false,
	});
	const [focused, setFocused] = useState([]);
	const [errors, setErrors] = useState({});
	const [submitDisabled, setSubmitDisabled] = useState(true);
	const navigate = useNavigate();

	const validateInput = () => {
		const temp = { ...errors };

		temp.name = formData.name ? "" : "This field is required.";
		temp.description = formData.description ? "" : "This field is required.";

		if (formData.img_url === "") {
			temp.img_url = "";
		} else {
			temp.img_url =
				/^(http(s?):\/\/.)[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/g.test(
					formData.img_url,
				)
					? ""
					: "Please enter a valid URL.";
		}

		if (JSON.stringify(errors) !== JSON.stringify(temp)) {
			setErrors({ ...temp });
			setSubmitDisabled(!Object.values(temp).every((x) => x === ""));
		}
	};

	const handleFocus = (e) => {
		e.preventDefault();
		const inputName = e.target.name;

		if (!focused.includes(inputName)) {
			setFocused([...focused, inputName]);
		}
	};

	const handleFormChange = (e) => {
		e.preventDefault();
		const value = e.target.value;
		const inputName = e.target.name;
		setFormData({
			...formData,
			[inputName]: value,
		});
	};

	const handleSwitchChange = (e) => {
		e.preventDefault();
		if (formData.published) {
			setFormData({ ...formData, published: false });
		} else {
			setFormData({ ...formData, published: true });
		}
	};

	const handleFormSubmission = async (e) => {
		e.preventDefault();

		if (!authState.authenticated) {
			toast.error("You must be logged in.");
			navigate({ to: "/login" });
		} else if (authState.role < 1) {
			toast.error("You do not have permission for this action.");
			navigate({ to: "/wiki" });
		} else {
			const temp = { ...formData };
			if (temp.img_url === "") {
				temp.img_url = null;
			}

			let url;
			let fetchConfig;

			if (typeof uuid === "string") {
				url = `${import.meta.env.VITE_API_HOST}/teas/${uuid}`;
				fetchConfig = {
					method: "put",
					body: JSON.stringify(temp),
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				};
			} else {
				url = `${import.meta.env.VITE_API_HOST}/teas`;
				fetchConfig = {
					method: "post",
					body: JSON.stringify(temp),
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				};
			}

			const response = await fetch(url, fetchConfig);

			if (response.ok) {
				authState.role === 2
					? toast.success("Tea submitted!")
					: toast.success("Tea submitted for review!");
				if (typeof uuid === "string") {
					navigate({ to: "/wiki/teas/$uuid", params: { uuid: uuid } });
				} else {
					setFormData({
						name: "",
						description: "",
						img_url: "",
						published: false,
					});
					setFocused([]);
				}
			} else {
				const detail = await response.json();
				toast.error(`An error occurred. ${detail.message}`);
				if (response.status === 401) {
					setAuthState(unauthenticated);
				}
			}
		}
	};

	useEffect(() => {
		const getTea = async () => {
			if (typeof uuid === "string") {
				const response = await fetch(
					`${import.meta.env.VITE_API_HOST}/teas/${uuid}/`,
				);
				if (response.ok) {
					const data = await response.json();
					if (!data.img_url) {
						data.img_url = "";
					}
					setFormData({ ...data });
				}
			}
		};
		getTea();
	}, [uuid]);

	useEffect(() => {
		validateInput();
	});

	return (
		<Container component="main" maxWidth="xs">
			<Paper
				sx={{
					marginTop: 15,
					marginBottom: 5,
					display: "flex",
					flexDirection: "column",
					alignItems: "flex-end",
				}}
				elevation={8}
			>
				<IconButton
					component={Link}
					to="/wiki"
					aria-label="return to wiki home"
				>
					<CloseIcon />
				</IconButton>
				<Box
					sx={{
						paddingTop: 0,
						paddingX: 3,
						paddingBottom: 4,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}
				>
					<Typography component="h1" variant="h5">
						{typeof uuid === "string"
							? authState.role === 2
								? `Edit ${formData.name}`
								: `Suggest a Change to ${formData.name}`
							: authState.role === 2
								? "Submit a New Tea"
								: "Suggest a New Tea"}
					</Typography>
					{authState.role !== 2 && (
						<Typography
							component="p"
							variant="body2"
							mt="1em"
							mx="3em"
							textAlign="center"
						>
							Fill out this form to submit a tea suggestion to the
							administrative team.
						</Typography>
					)}
					<Box component="form" onSubmit={handleFormSubmission} sx={{ mt: 3 }}>
						<Grid container spacing={2} display="flex" alignItems="center">
							<Grid item xs={12}>
								<TextField
									disabled={typeof uuid === "string"}
									variant="outlined"
									required
									fullWidth
									onChange={handleFormChange}
									onFocus={handleFocus}
									name="name"
									id="name"
									label="Tea Name"
									value={formData.name}
									error={
										focused.includes("name") ? Boolean(errors.name) : false
									}
									helperText={focused.includes("name") ? errors.name : false}
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									variant="outlined"
									multiline
									minRows={4}
									required
									fullWidth
									onChange={handleFormChange}
									onFocus={handleFocus}
									name="description"
									id="description"
									label="Tea Description"
									value={formData.description}
									error={
										focused.includes("description")
											? Boolean(errors.description)
											: false
									}
									helperText={
										focused.includes("description") ? errors.description : false
									}
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									variant="outlined"
									fullWidth
									onFocus={handleFocus}
									onChange={handleFormChange}
									name="img_url"
									id="img_url"
									label="Image URL"
									value={formData.img_url}
									error={Boolean(errors.img_url)}
									helperText={errors.img_url}
								/>
							</Grid>
							{authState.role === 2 && (
								<Grid
									item
									display="flex"
									justifyContent="space-around"
									alignItems="center"
									mx={5}
									xs={12}
									sx={{ width: 1 }}
								>
									<Typography variant="subtitle1">
										Set Tea to Published:
									</Typography>
									<Switch
										aria-label="publish tea switch"
										name="published"
										checked={formData.published}
										onClick={handleSwitchChange}
									/>
								</Grid>
							)}
						</Grid>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							disabled={Boolean(submitDisabled)}
							sx={{ mt: 3 }}
						>
							Submit
						</Button>
						{submitDisabled && (
							<Typography component="p" variant="caption" align="center">
								Please satisfy all form requirements.
							</Typography>
						)}
					</Box>
				</Box>
			</Paper>
		</Container>
	);
}
