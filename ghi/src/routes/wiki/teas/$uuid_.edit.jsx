import { createFileRoute, redirect, useParams } from "@tanstack/react-router";
import TeaForm from "./-TeaForm";

export const Route = createFileRoute("/wiki/teas/$uuid/edit")({
	component: EditTeaForm,
	beforeLoad: ({ context, location }) => {
		if (!context.authState.authenticated) {
			throw redirect({
				to: "/login",
				search: {
					redirect: location.href,
				},
			});
		}
	},
});

function EditTeaForm() {
	const params = useParams({ from: "/wiki/teas/$uuid/edit" });
	return TeaForm(params.uuid);
}
