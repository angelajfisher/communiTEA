import CheckIcon from "@mui/icons-material/Check";
import CloseIcon from "@mui/icons-material/Close";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import EditIcon from "@mui/icons-material/Edit";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import {
	Accordion,
	AccordionDetails,
	AccordionSummary,
	Avatar,
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogTitle,
	IconButton,
	Paper,
	Stack,
	Tab,
	Tooltip,
	Typography,
} from "@mui/material";
import { Link, createFileRoute, redirect, useNavigate } from "@tanstack/react-router";
import { useCallback, useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "../../utils";

export const Route = createFileRoute("/wiki/manage")({
	component: AdminPanel,
	beforeLoad: ({ context, location }) => {
		if (!context.authState.authenticated) {
			throw redirect({
				to: "/login",
				search: {
					redirect: location.href,
				},
			});
		}
	},
});

function AdminPanel() {
	const [authState, setAuthState] = useContext(AuthContext);
	const [currentTab, setCurrentTab] = useState("1");
	const [expanded, setExpanded] = useState(false);
	const [pendingSubmissions, setPendingSubmissions] = useState([]);
	const [publishedTeas, setPublishedTeas] = useState([]);
	const [unpublishedTeas, setUnpublishedTeas] = useState([]);
	const [delTeaDialogStatus, setTeaDelDialogStatus] = useState(false);
	const [pubTeaDialogStatus, setTeaPubDialogStatus] = useState(false);
	const [acceptSubDialogStatus, setAcceptSubDialogStatus] = useState(false);
	const [denySubDialogStatus, setDenySubDialogStatus] = useState(false);
	const navigate = useNavigate({ from: Route.fullPath });

	const authenticate = useCallback(() => {
		toast.error("You are not authorized to complete this action.");
		navigate("/");
	}, [navigate]);

	const handleTabSelect = (e, newValue) => {
		e.preventDefault();
		setCurrentTab(newValue);
	};

	const handlePanelChange = (panel) => (e, isExpanded) => {
		e.preventDefault();
		setExpanded(isExpanded ? panel : false);
	};

	const handleDeleteTea = async (teaUuid) => {
		if (authState.authenticated) {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/${teaUuid}/`,
				{
					method: "delete",
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				},
			);
			if (response.ok) {
				toast.success("Tea successfully deleted.");
				getPublishedTeas();
				getUnpublishedTeas();
			} else {
				toast.error("Could not delete tea.");
				setTeaDelDialogStatus(false);
			}
		} else {
			authenticate();
		}
	};

	const handlePublishTea = async (teaUuid) => {
		if (authState.authenticated) {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/${teaUuid}/publish/`,
				{
					method: "put",
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				},
			);
			if (response.ok) {
				toast.success("Tea successfully published.");
				getPublishedTeas();
				getUnpublishedTeas();
			} else {
				const detail = await response.json();
				toast.error(`An error occurred. ${detail.message}`);
				if (response.status === 401) {
					setAuthState(unauthenticated);
				}
				setTeaPubDialogStatus(false);
			}
		} else {
			authenticate();
		}
	};

	const handleAcceptSub = async (draft) => {
		if (authState.authenticated) {
			const tea_edit = {
				name: draft.name,
				description: draft.description,
				img_url: draft.img_url,
			};
			const teaResponse = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/${draft.tea_uuid}/`,
				{
					method: "put",
					body: JSON.stringify(tea_edit),
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				},
			);
			if (teaResponse.ok) {
				const delResponse = await fetch(
					`${import.meta.env.VITE_API_HOST}/teas/submissions/${
						draft.draft_uuid
					}/`,
					{
						method: "delete",
						headers: {
							"Content-Type": "application/json",
						},
						credentials: "include",
					},
				);
				if (delResponse.ok) {
					toast.success("Submission successfully accepted.");
					getPendingSubmissions();
				} else {
					const detail = await delResponse.json();
					toast.error(`Could not log in. ${detail.message}`);
					setAuthState(unauthenticated);
					setAcceptSubDialogStatus(false);
				}
			} else {
				toast.error("Error accepting submission.");
				setAcceptSubDialogStatus(false);
			}
		} else {
			authenticate();
		}
	};

	const handleDenySub = async (draftUuid) => {
		if (authState.authenticated) {
			const response = await fetch(
				`${import.meta.env.VITE_API_HOST}/teas/submissions/${draftUuid}/`,
				{
					method: "delete",
					headers: {
						"Content-Type": "application/json",
					},
					credentials: "include",
				},
			);
			if (response.ok) {
				toast.success("Draft successfully denied.");
				getPendingSubmissions();
			} else {
				toast.error("Error denying draft.");
				setDenySubDialogStatus(false);
			}
		} else {
			authenticate();
		}
	};

	const handleEditTea = (teaUuid) => {
		if (authState.authenticated) {
			navigate({ to: "/wiki/teas/$uuid/edit", params: { uuid: teaUuid } });
		} else {
			authenticate();
		}
	};

	const getPublishedTeas = useCallback(async () => {
		const response = await fetch(`${import.meta.env.VITE_API_HOST}/teas/`);
		if (response.ok) {
			const data = await response.json();
			setPublishedTeas(data);
		}
	}, []);

	const getUnpublishedTeas = useCallback(async () => {
		const response = await fetch(
			`${import.meta.env.VITE_API_HOST}/teas/unpublished/`,
			{
				method: "get",
				headers: {
					"Content-Type": "application/json",
				},
				credentials: "include",
			},
		);
		if (response.ok) {
			const data = await response.json();
			setUnpublishedTeas(data);
		}
	}, []);

	const getPendingSubmissions = useCallback(async () => {
		const response = await fetch(
			`${import.meta.env.VITE_API_HOST}/teas/submissions/`,
			{
				method: "get",
				headers: {
					"Content-Type": "application/json",
				},
				credentials: "include",
			},
		);
		if (response.ok) {
			const data = await response.json();
			setPendingSubmissions(data);
		}
	}, []);

	useEffect(() => {
		if (authState.authenticated) {
			getPendingSubmissions();
			getPublishedTeas();
			getUnpublishedTeas();
		} else {
			authenticate();
		}
	}, [
		authState.authenticated,
		getPendingSubmissions,
		getPublishedTeas,
		getUnpublishedTeas,
		authenticate,
	]);

	return (
		<Paper elevation={4} sx={{ p: 2, m: 2, mt: 10 }}>
			<Box sx={{ bgcolor: "background.paper", py: 2 }}>
				<Typography variant="h3">Manage Teas</Typography>
				<Link to="/wiki/teas/create">
					<Button variant="contained">Add a New Tea</Button>
				</Link>
				<TabContext value={currentTab}>
					<Box
						sx={{
							paddingTop: 3,
							borderBottom: 1,
							borderColor: "divider",
						}}
					>
						<TabList onChange={handleTabSelect}>
							<Tab
								value="1"
								label="Pending Submissions"
								id="pending-submissions-tab"
							/>
							<Tab value="2" label="Published Teas" id="published-teas-tab" />
							<Tab
								value="3"
								label="Unpublished Teas"
								id="unpublished-teas-tab"
							/>
						</TabList>
					</Box>

					{/* Pending Submissions */}
					<TabPanel value="1">
						{pendingSubmissions.length > 0 ? (
							pendingSubmissions.map((tea) => (
								<Accordion
									key={tea.name}
									expanded={expanded === tea.name}
									onChange={handlePanelChange(tea.name)}
								>
									<AccordionSummary expandIcon={<ExpandMoreIcon />}>
										<Stack
											direction="row"
											alignItems="center"
											justifyContent="space-between"
											sx={{ width: 1 }}
										>
											<Typography
												variant="h6"
												sx={{
													width: 1,
													paddingLeft: 3,
												}}
											>
												{tea.name}
											</Typography>
											<Stack direction="row">
												<Tooltip title="Accept Submission">
													<IconButton
														onClick={(e) => setAcceptSubDialogStatus(tea.name)}
														p={0}
														aria-label="accept submission"
													>
														<CheckIcon fontSize="small" />
													</IconButton>
												</Tooltip>
												<Dialog
													open={acceptSubDialogStatus === tea.name}
													onClose={(e) => setAcceptSubDialogStatus(false)}
													aria-labelledby="alert-dialog-title"
													aria-describedby="alert-dialog-description"
												>
													<DialogTitle id="alert-dialog-title">
														{`Accept submission for ${tea.name}? This will replace its current information!`}
													</DialogTitle>
													<DialogActions>
														<Button onClick={(e) => handleAcceptSub(tea)}>
															Yes, Accept
														</Button>
														<Button
															autoFocus
															onClick={(e) => setAcceptSubDialogStatus(false)}
															sx={{
																color: "error",
															}}
														>
															Cancel
														</Button>
													</DialogActions>
												</Dialog>
												<Tooltip title="Deny Submission">
													<IconButton
														onClick={(e) => setDenySubDialogStatus(tea.name)}
														p={0}
														aria-label="deny submission"
													>
														<CloseIcon fontSize="small" />
													</IconButton>
												</Tooltip>
												<Dialog
													open={denySubDialogStatus === tea.name}
													onClose={(e) => setDenySubDialogStatus(false)}
													aria-labelledby="alert-dialog-title"
													aria-describedby="alert-dialog-description"
												>
													<DialogTitle id="alert-dialog-title">
														{`Deny submission for ${tea.name}? This action is irreversible!`}
													</DialogTitle>
													<DialogActions>
														<Button
															onClick={(e) => handleDenySub(tea.draft_uuid)}
														>
															Yes, Deny
														</Button>
														<Button
															autoFocus
															onClick={(e) => setDenySubDialogStatus(false)}
															sx={{
																color: "error",
															}}
														>
															Cancel
														</Button>
													</DialogActions>
												</Dialog>
											</Stack>
										</Stack>
									</AccordionSummary>
									<AccordionDetails>
										<Stack direction="row">
											<Avatar
												alt={tea.name}
												src={tea.img_url}
												variant="square"
												sx={{ width: 150, height: 150 }}
											/>
											<Typography variant="body1" sx={{ px: 5 }}>
												{tea.description}
											</Typography>
										</Stack>
									</AccordionDetails>
								</Accordion>
							))
						) : (
							<Typography>There are no pending submissions.</Typography>
						)}
					</TabPanel>

					{/* Published Teas */}
					<TabPanel value="2">
						{publishedTeas.map((tea) => (
							<Accordion
								key={tea.name}
								expanded={expanded === tea.name}
								onChange={handlePanelChange(tea.name)}
							>
								<AccordionSummary expandIcon={<ExpandMoreIcon />}>
									<Stack
										direction="row"
										alignItems="center"
										justifyContent="space-between"
										sx={{ width: 1 }}
									>
										<Typography variant="h6" sx={{ width: 1, paddingLeft: 3 }}>
											{tea.name}
										</Typography>
										<Stack direction="row">
											<Tooltip title="Edit Tea">
												<IconButton
													onClick={(e) => handleEditTea(tea.uuid)}
													p={0}
													aria-label="edit tea"
												>
													<EditIcon fontSize="small" />
												</IconButton>
											</Tooltip>
											<Tooltip title="Delete Tea">
												<IconButton
													onClick={(e) => setTeaDelDialogStatus(tea.name)}
													p={0}
													aria-label="delete tea"
												>
													<DeleteForeverIcon fontSize="small" />
												</IconButton>
											</Tooltip>
											<Dialog
												open={delTeaDialogStatus === tea.name}
												onClose={(e) => setTeaDelDialogStatus(false)}
												aria-labelledby="alert-dialog-title"
												aria-describedby="alert-dialog-description"
											>
												<DialogTitle id="alert-dialog-title">
													{`Delete ${tea.name}? This action is irreversible!`}
												</DialogTitle>
												<DialogActions>
													<Button onClick={(e) => handleDeleteTea(tea.uuid)}>
														Yes, Delete
													</Button>
													<Button
														autoFocus
														onClick={(e) => setTeaDelDialogStatus(false)}
														sx={{ color: "error" }}
													>
														Cancel
													</Button>
												</DialogActions>
											</Dialog>
										</Stack>
									</Stack>
								</AccordionSummary>
								<AccordionDetails>
									<Stack direction="row">
										<Avatar
											alt={tea.name}
											src={tea.img_url}
											variant="square"
											sx={{ width: 150, height: 150 }}
										/>
										<Typography variant="body1" sx={{ px: 5 }}>
											{tea.description}
										</Typography>
									</Stack>
								</AccordionDetails>
							</Accordion>
						))}
					</TabPanel>

					{/* Unpublished Teas */}
					<TabPanel value="3">
						{unpublishedTeas.length > 0 ? (
							unpublishedTeas.map((tea) => (
								<Accordion
									key={tea.name}
									expanded={expanded === tea.name}
									onChange={handlePanelChange(tea.name)}
								>
									<AccordionSummary expandIcon={<ExpandMoreIcon />}>
										<Stack
											direction="row"
											alignItems="center"
											justifyContent="space-between"
											sx={{ width: 1 }}
										>
											<Typography
												variant="h6"
												sx={{
													width: 1,
													paddingLeft: 3,
												}}
											>
												{tea.name}
											</Typography>
											<Stack direction="row">
												<Tooltip title="Publish">
													<IconButton
														onClick={(e) => setTeaPubDialogStatus(tea.name)}
														p={0}
														aria-label="publish tea"
													>
														<CheckIcon fontSize="small" />
													</IconButton>
												</Tooltip>
												<Dialog
													open={pubTeaDialogStatus === tea.name}
													onClose={(e) => setTeaPubDialogStatus(false)}
													aria-labelledby="alert-dialog-title"
													aria-describedby="alert-dialog-description"
												>
													<DialogTitle id="alert-dialog-title">
														{`Publish ${tea.name}? This will make it visible to the public!`}
													</DialogTitle>
													<DialogActions>
														<Button onClick={(e) => handlePublishTea(tea.uuid)}>
															Yes, Publish
														</Button>
														<Button
															autoFocus
															onClick={(e) => setTeaPubDialogStatus(false)}
															sx={{
																color: "error",
															}}
														>
															Cancel
														</Button>
													</DialogActions>
												</Dialog>
												<Tooltip title="Edit Tea">
													<IconButton
														onClick={(e) => handleEditTea(tea.uuid)}
														p={0}
														aria-label="edit tea"
													>
														<EditIcon fontSize="small" />
													</IconButton>
												</Tooltip>
												<Tooltip title="Delete Tea">
													<IconButton
														onClick={(e) => setTeaDelDialogStatus(tea.name)}
														p={0}
														aria-label="delete tea"
													>
														<DeleteForeverIcon fontSize="small" />
													</IconButton>
												</Tooltip>
												<Dialog
													open={delTeaDialogStatus === tea.name}
													onClose={(e) => setTeaDelDialogStatus(false)}
													aria-labelledby="alert-dialog-title"
													aria-describedby="alert-dialog-description"
												>
													<DialogTitle id="alert-dialog-title">
														{`Delete ${tea.name}? This action is irreversible!`}
													</DialogTitle>
													<DialogActions>
														<Button onClick={(e) => handleDeleteTea(tea.uuid)}>
															Yes, Delete
														</Button>
														<Button
															autoFocus
															onClick={(e) => setTeaDelDialogStatus(false)}
															sx={{
																color: "error",
															}}
														>
															Cancel
														</Button>
													</DialogActions>
												</Dialog>
											</Stack>
										</Stack>
									</AccordionSummary>
									<AccordionDetails>
										<Stack direction="row">
											<Avatar
												alt={tea.name}
												src={tea.img_url}
												variant="square"
												sx={{ width: 150, height: 150 }}
											/>
											<Typography variant="body1" sx={{ px: 5 }}>
												{tea.description}
											</Typography>
										</Stack>
									</AccordionDetails>
								</Accordion>
							))
						) : (
							<Typography>There are no unpublished teas.</Typography>
						)}
					</TabPanel>
				</TabContext>
			</Box>
		</Paper>
	);
}
