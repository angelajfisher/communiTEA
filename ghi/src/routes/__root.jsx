import { Outlet, createRootRoute } from "@tanstack/react-router";
import { Suspense, lazy, useContext } from "react";
import { AuthContext } from "../utils";
import Navbar from "./-Navbar";
import PageNotFound from "./../PageNotFound";
import { WelcomeDialog } from "./../WelcomeDialog";

const TSRDevtools =
	process.env.NODE_ENV === "production"
		? () => null // Render nothing in production
		: lazy(() =>
				// Lazy load in development
				import("@tanstack/router-devtools").then((res) => ({
					default: res.TanStackRouterDevtools,
				})),
			);

export const Route = createRootRoute({
	component: RootComponent,
	notFoundComponent: PageNotFound,
});

function RootComponent() {
	const [authState] = useContext(AuthContext);
	const hideWelcome = localStorage.getItem("noDemo");
	return (
		<>
			{!authState.authenticated &&
				(!hideWelcome || hideWelcome === "false") && <WelcomeDialog />}
			<Navbar />
			<Outlet />
			<Suspense>
				<TSRDevtools />
			</Suspense>
		</>
	);
}
