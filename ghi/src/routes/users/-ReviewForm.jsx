import { Box, Button, TextField } from "@mui/material";
import { useNavigate } from "@tanstack/react-router";
import { useContext, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "../../utils";

const ReviewForm = ({ uuid }) => {
	const [authState, setAuthState] = useContext(AuthContext);
	const [rating, setRating] = useState("");
	const [comment, setComment] = useState("");
	const navigate = useNavigate();

	const handleEventForm = async (e) => {
		e.preventDefault();
		if (rating < 1 || rating > 5) {
			toast.error("You must insert a value (1-5)");
			return;
		}

		const reviewData = {
			rating: rating,
			comment: comment,
		};

		const ReviewUrl = `${import.meta.env.VITE_API_HOST}/businesses/${uuid}/reviews/`;
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(reviewData),
			headers: {
				"Content-Type": "application/json",
			},
			credentials: "include",
		};

		const response = await fetch(ReviewUrl, fetchConfig);
		if (response.ok) {
			toast.success("Review submitted!");
			setRating("");
			setComment("");
			navigate({ to: "/users/$uuid", params: { uuid: uuid } });
		} else {
			const detail = await response.json();
			toast.error(`An error occurred. ${detail.message}`);
			if (response.status === 401) {
				setAuthState(unauthenticated);
			}
		}
	};

	return (
		<form onSubmit={handleEventForm}>
			<Box mb={2}>
				<TextField
					fullWidth={false}
					sx={{ width: 100 }}
					variant="outlined"
					label="Rating"
					type="number"
					value={rating}
					onChange={(e) => setRating(Math.min(Math.max(e.target.value, 1), 5))}
				/>
			</Box>
			<Box mb={2}>
				<TextField
					fullWidth
					variant="outlined"
					label="Comment"
					multiline
					rows={4}
					value={comment}
					onChange={(e) => setComment(e.target.value)}
				/>
			</Box>
			<Button variant="contained" color="primary" type="submit">
				Submit Review
			</Button>
		</form>
	);
};

export default ReviewForm;
