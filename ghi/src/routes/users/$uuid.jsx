import {
	Avatar,
	Box,
	Container,
	Paper,
	Rating,
	Typography,
} from "@mui/material";
import { createFileRoute, useParams } from "@tanstack/react-router";
import { useEffect, useState } from "react";
import ReviewForm from "./-ReviewForm";

export const Route = createFileRoute("/users/$uuid")({
	component: UserProfile,
});

function UserProfile() {
	const [user, setBusinessUsers] = useState({});
	const [reviews, setReviews] = useState([]);
	const [authors, setAuthors] = useState({});
	const params = useParams({ from: "/users/$uuid" });

	useEffect(() => {
		const fetchData = async () => {
			const userRes = await fetch(
				`${import.meta.env.VITE_API_HOST}/users/${params.uuid}/`,
			);
			let userData;
			if (userRes.ok) {
				userData = await userRes.json();
				setBusinessUsers(userData);
			}
			if (userData.role === 1) {
				const reviewRes = await fetch(
					`${import.meta.env.VITE_API_HOST}/businesses/${params.uuid}/reviews/`,
				);
				if (reviewRes.ok) {
					const data = await reviewRes.json();
					setReviews(data);

					for (const review of data) {
						const reviewAuthors = await fetch(
							`${import.meta.env.VITE_API_HOST}/users/${review.author}/`,
						);
						if (reviewAuthors.ok) {
							const userData = await reviewAuthors.json();
							setAuthors((prevAuthors) => ({
								...prevAuthors,
								[review.author]: userData,
							}));
						}
					}
				}
			}
		};
		fetchData();
	}, [params.uuid]);

	return (
		<Container sx={{ mt: 15 }}>
			<Paper elevation={3} sx={{ p: 4, mb: 3 }}>
				<Box sx={{ textAlign: "center" }}>
					<img
						src={user.img_url}
						alt={`${user.display_name}'s avatar`}
						width="200"
						height="200"
						style={{ borderRadius: "50%" }}
					/>
					<Typography variant="h4" mt={3}>
						{user.display_name}
					</Typography>
					<Typography mt={2} fontSize="1.2rem">
						{user.description}
					</Typography>
					<Typography mt={2} fontSize="1.2rem">
						{user.hours}
					</Typography>
					<Typography mt={2} fontSize="1.2rem">
						{user.location}
					</Typography>
				</Box>
				<Box mt={3}>
					{reviews.map((review) => (
						<Paper
							key={review.uuid}
							elevation={2}
							sx={{
								p: 2,
								mt: 2,
								display: "flex",
								alignItems: "center",
							}}
						>
							<Avatar
								src={authors[review.author]?.img_url || ""}
								alt="User Reviewer"
							/>
							<Box ml={2}>
								<Typography variant="h6">
									{authors[review.author]?.display_name || "Unknown User"}
								</Typography>
								<Typography variant="body2">{review.comment}</Typography>
								<Rating name="read-only" value={review.rating} readOnly />
							</Box>
						</Paper>
					))}
				</Box>
				{user?.role === 1 && (
					<Box mt={4}>
						<ReviewForm uuid={params.uuid} />
					</Box>
				)}
			</Paper>
		</Container>
	);
}
