import { useTheme } from "@emotion/react";
import {
	Box,
	Button,
	Container,
	Grid,
	Paper,
	Typography,
	useMediaQuery,
} from "@mui/material";
import { Link, createFileRoute } from "@tanstack/react-router";

export const Route = createFileRoute("/")({
	component: MainPage,
});

function MainPage() {
	const theme = useTheme();
	const boxStyling = useMediaQuery(theme.breakpoints.down("md"))
		? {
				position: "relative",
				p: 5,
				height: "20em",
			}
		: {
				position: "relative",
				px: {md: 10, xl: 45},
				py: 45
			};

	return (
		<>
			<Paper
				square
				sx={{
					position: "relative",
					color: "#fff",
					mt: 7,
					mb: 3,
					backgroundSize: "cover",
					backgroundRepeat: "no-repeat",
					backgroundPosition: "center",
					backgroundImage: `url("https://images.pexels.com/photos/6913383/pexels-photo-6913383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")`,
				}}
			>
				<Grid container sx={{ backdropFilter: "brightness(80%)", whiteSpace: "nowrap" }}>
					<Grid item md={7}>
						<Box sx={boxStyling}>
							<Typography
								component="h1"
								variant="h3"
								color="inherit"
								gutterBottom
							>
								CommuniTEA
							</Typography>
							<Button component={Link} variant="contained" to="/signup">
								Get Started
							</Button>
						</Box>
					</Grid>
				</Grid>
			</Paper>
			<main>
				<Box sx={{ pt: 2, pb: 5 }}>
					<Container maxWidth="lg">
						<Typography
							style={{ fontSize: "18px" }}
							align="center"
							color="black"
						>
							Join the CommuniTea! Here, you can meet people who share your
							interest, talk about all things tea, and learn more through
							different events. Whether you know a lot about tea or just like
							drinking it, there&apos;s something for you! You can find a tea
							store near you or you can attend events to find out more about how
							tea is made, its history, and how to enjoy it in new ways.
							Everyone is welcome. It&apos;s a simple way to enjoy tea with
							others and maybe learn something new. So, if you like tea, come be
							a part of the CommuniTea!
						</Typography>
					</Container>
				</Box>
			</main>
		</>
	);
}
