import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import {
	Avatar,
	Box,
	Button,
	CssBaseline,
	Grid,
	Paper,
	TextField,
	Typography,
} from "@mui/material";
import {
	Link,
	createFileRoute,
	useNavigate,
	useRouter,
} from "@tanstack/react-router";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "../utils";

export const Route = createFileRoute("/login")({
	component: LoginForm,
	validateSearch: (search) => {
		return {
			redirect: search.redirect,
		};
	},
});

function LoginForm() {
	const [authState, setAuthState] = useContext(AuthContext);
	const [formData, setFormData] = useState({
		username: "",
		password: "",
	});
	const search = Route.useSearch();
	const router = useRouter();
	const navigate = useNavigate({ from: Route.fullPath });

	useEffect(() => {
		if (authState.authenticated) {
			if (search.redirect) {
				router.history.push(search.redirect);
			} else {
				navigate({ to: "/" });
			}
		}
	}, [authState.authenticated, search.redirect, router.history, navigate]);

	const handleSubmit = async (e) => {
		e.preventDefault();
		const response = await fetch(`${import.meta.env.VITE_API_HOST}/signin/`, {
			method: "post",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json",
			},
			credentials: "include",
		});
		if (response.ok) {
			const userData = await response.json();
			setAuthState({ authenticated: true, ...userData });
			toast.success(`Welcome back, ${userData.display_name}!`);
		} else {
			const detail = await response.json();
			toast.error(`Could not log in. ${detail.message}`);
			setAuthState(unauthenticated);
		}
	};

	return (
		<Grid container component="main" sx={{ height: "100vh" }}>
			<CssBaseline />
			<Grid
				item
				xs={false}
				sm={4}
				md={7}
				sx={{
					backgroundImage:
						"url(https://images.pexels.com/photos/6913383/pexels-photo-6913383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2)",
					backgroundRepeat: "no-repeat",
					backgroundColor: (t) =>
						t.palette.mode === "light"
							? t.palette.grey[50]
							: t.palette.grey[900],
					backgroundSize: "cover",
					backgroundPosition: "center",
				}}
			/>
			<Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
				<Box
					sx={{
						my: 17,
						mx: 4,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}
				>
					<Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					<Box
						component="form"
						noValidate
						onSubmit={handleSubmit}
						sx={{ mt: 1 }}
					>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="username"
							label="Username"
							name="username"
							autoComplete="username"
							autoFocus
							onChange={(e) =>
								setFormData({ ...formData, username: e.target.value })
							}
						/>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
							onChange={(e) =>
								setFormData({ ...formData, password: e.target.value })
							}
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							sx={{ mt: 3, mb: 2 }}
						>
							Sign In
						</Button>
						<Grid container>
							<Grid item>
								<Link
									style={{ textDecoration: "underline", color: "gray" }}
									to="/signup"
									variant="body2"
								>
									{"Don't have an account? Sign Up"}
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</Grid>
		</Grid>
	);
}
