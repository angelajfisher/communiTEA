import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {
	Avatar,
	Box,
	Button,
	Container,
	FormControl,
	FormHelperText,
	Grid,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
	Paper,
	Switch,
	TextField,
	Typography,
} from "@mui/material";
import {
	Link,
	createFileRoute,
	redirect,
	useNavigate,
} from "@tanstack/react-router";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { AuthContext, unauthenticated } from "../utils";

export const Route = createFileRoute("/signup")({
	component: SignupForm,
	validateSearch: (search) => {
		return { demo: search.demo === true };
	},
	beforeLoad: ({ context }) => {
		if (context.authState.authenticated) {
			throw redirect({
				to: "/",
			});
		}
	},
});

function SignupForm() {
	const { demo } = Route.useSearch();
	const [authState, setAuthState] = useContext(AuthContext);
	const [formData, setFormData] = useState({
		username: "",
		password: "",
		role: demo,
		img_url: "",
		display_name: "",
		description: "",
		hours: "",
		location: demo ? "Seattle, WA" : "",
	});
	const [focused, setFocused] = useState([]);
	const [errors, setErrors] = useState({});
	const [showPassword, setShowPassword] = useState(false);
	const [submitDisabled, setSubmitDisabled] = useState(true);
	const navigate = useNavigate({ from: Route.fullPath });

	const handleClickShowPassword = () => setShowPassword((show) => !show);

	const handleMouseDownPassword = (event) => {
		event.preventDefault();
	};

	const validateInput = () => {
		const required = "This field is required.";
		const temp = { ...errors };

		temp.username = formData.username ? "" : required;
		temp.password = formData.password ? "" : required;
		temp.location = formData.location ? "" : required;

		if (formData.img_url === "") {
			temp.img_url = "";
		} else {
			temp.img_url =
				/^(http(s?):\/\/.)[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/g.test(
					formData.img_url,
				)
					? ""
					: "Please enter a valid URL.";
		}

		if (formData.location !== "") {
			temp.location = /[A-Z][a-zA-Z]+,[ ]?[A-Z]{2}/gm.test(formData.location)
				? ""
				: "Please enter a valid city and state abbreviation. Field is case-sensitive.";
		}

		if (JSON.stringify(errors) !== JSON.stringify(temp)) {
			setErrors({ ...temp });
			setSubmitDisabled(!Object.values(temp).every((x) => x === ""));
		}
	};

	const handleFocus = (e) => {
		e.preventDefault();
		const inputName = e.target.name;

		if (!focused.includes(inputName)) {
			setFocused([...focused, inputName]);
		}
	};

	const handleFormChange = (e) => {
		e.preventDefault();
		const value = e.target.value;
		const inputName = e.target.name;
		setFormData({
			...formData,
			[inputName]: value,
		});
	};

	const handleSwitchChange = (e) => {
		e.preventDefault();
		if (formData.role) {
			setFormData({ ...formData, role: false });
		} else {
			setFormData({ ...formData, role: true });
		}
	};

	const handleRegistration = async (e) => {
		e.preventDefault();

		let accountData;
		if (formData.role) {
			accountData = { ...formData, role: 1 };
		} else {
			accountData = { ...formData, role: 0 };
		}
		if (formData.display_name === "") {
			accountData = { ...accountData, display_name: accountData.username };
		}

		const response = await fetch(`${import.meta.env.VITE_API_HOST}/users/`, {
			method: "post",
			body: JSON.stringify(accountData),
			headers: {
				"Content-Type": "application/json",
			},
			credentials: "include",
		});
		if (response.ok) {
			const userData = await response.json();
			setAuthState({ authenticated: true, ...userData });
			toast.success(`Welcome in, ${userData.display_name}!`);
			navigate({ to: "/" });
		} else {
			const detail = await response.json();
			toast.error(`An error occurred! ${detail.message}`);
			setAuthState(unauthenticated);
		}
	};

	useEffect(() => {
		validateInput();
	});

	return (
		<Container component="main" maxWidth="xs">
			<Paper
				sx={{
					marginTop: 15,
					marginBottom: 5,
					paddingTop: 2,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
				elevation={8}
			>
				<Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
					<LockOutlinedIcon />
				</Avatar>
				<Box
					sx={{
						paddingTop: 0,
						paddingX: 3,
						paddingBottom: 4,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}
				>
					<Typography component="h1" variant="h5">
						Sign Up
					</Typography>
					<Box component="form" onSubmit={handleRegistration} sx={{ mt: 3 }}>
						<Grid container spacing={2}>
							<Grid item xs={12}>
								<TextField
									variant="outlined"
									required
									fullWidth
									onChange={handleFormChange}
									onFocus={handleFocus}
									name="username"
									id="username"
									label="Username"
									value={formData.username}
									error={
										focused.includes("username")
											? Boolean(errors.username)
											: false
									}
									helperText={
										focused.includes("username") ? errors.username : false
									}
								/>
							</Grid>
							<Grid item xs={12}>
								<FormControl
									error={
										focused.includes("password")
											? Boolean(errors.password)
											: false
									}
									variant="outlined"
									sx={{ width: 1 }}
								>
									<InputLabel htmlFor="password">Password *</InputLabel>
									<OutlinedInput
										onChange={handleFormChange}
										onFocus={handleFocus}
										name="password"
										id="password"
										type={showPassword ? "text" : "password"}
										endAdornment={
											<InputAdornment position="end">
												<IconButton
													aria-label="toggle password visibility"
													onClick={handleClickShowPassword}
													onMouseDown={handleMouseDownPassword}
													edge="end"
												>
													{showPassword ? <VisibilityOff /> : <Visibility />}
												</IconButton>
											</InputAdornment>
										}
										label="Password"
									/>
									<FormHelperText id="component-error-text">
										{focused.includes("password") ? errors.password : false}
									</FormHelperText>
								</FormControl>
							</Grid>
							<Grid item xs={12}>
								<TextField
									variant="outlined"
									fullWidth
									onFocus={handleFocus}
									onChange={handleFormChange}
									name="display_name"
									id="display_name"
									label="Display Name"
									value={formData.display_name}
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									variant="outlined"
									fullWidth
									onFocus={handleFocus}
									onChange={handleFormChange}
									name="img_url"
									id="img_url"
									label="Profile Image URL"
									value={formData.img_url}
									error={Boolean(errors.img_url)}
									helperText={errors.img_url}
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									variant="outlined"
									required
									fullWidth
									onChange={handleFormChange}
									onFocus={handleFocus}
									name="location"
									id="location"
									label="City, State Abbv."
									value={formData.location}
									error={
										focused.includes("location")
											? Boolean(errors.location)
											: false
									}
									helperText={
										focused.includes("location") ? errors.location : false
									}
								/>
							</Grid>
							<Grid
								item
								display="flex"
								justifyContent="space-around"
								alignItems="center"
								xs={12}
								marginX={10}
							>
								<Typography variant="subtitle1">Business User?</Typography>
								<Switch
									aria-label="publish tea switch"
									name="published"
									checked={formData.role}
									onClick={handleSwitchChange}
								/>
							</Grid>
							{formData.role && (
								<>
									<Grid item xs={12}>
										<TextField
											variant="outlined"
											fullWidth
											onFocus={handleFocus}
											onChange={handleFormChange}
											name="hours"
											id="hours"
											label="Business Hours"
											value={formData.hours}
										/>
									</Grid>
									<Grid item xs={12}>
										<TextField
											variant="outlined"
											multiline
											minRows={4}
											fullWidth
											onFocus={handleFocus}
											onChange={handleFormChange}
											name="description"
											id="description"
											label="Business Description"
											value={formData.description}
										/>
									</Grid>
								</>
							)}
						</Grid>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							disabled={submitDisabled}
							sx={{ mt: 3 }}
						>
							Submit
						</Button>
						{submitDisabled && (
							<Typography component="p" variant="caption" align="center">
								Please satisfy all form requirements.
							</Typography>
						)}
					</Box>
					<Grid container justifyContent="flex-end" paddingTop={3}>
						<Grid item>
							<Link
								style={{
									textDecoration: "underline",
									color: "gray",
								}}
								to="/login"
								variant="body2"
							>
								Already have an account? Sign in
							</Link>
						</Grid>
					</Grid>
				</Box>
			</Paper>
		</Container>
	);
}
