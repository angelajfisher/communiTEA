from fastapi import status
from fastapi.testclient import TestClient
from queries.businesses import BusinessRepository
from main import app

client = TestClient(app)


class TestLocationQueries:
    def get_businesses_by_location(self, location):
        return [
            {
                "uuid": "6c9f6441-d647-41fb-b551-e5bbcf18666d",
                "name": "alvinmach",
                "img_url": "Picture",
                "description": "Hoping this works",
                "hours": "5:00 AM - 5:00 PM",
                "location": "Los Angeles",
            },
            {
                "uuid": "4b3875cc-c689-4db0-bd86-e9319319de17",
                "name": "machalvin",
                "img_url": "Picture2",
                "description": "Hoping this works as well",
                "hours": "6:00 AM - 6:00 PM",
                "location": "Los Angeles",
            },
        ]


def test_get_businesses_by_location():
    app.dependency_overrides[BusinessRepository] = TestLocationQueries
    expected = [
        {
            "uuid": "6c9f6441-d647-41fb-b551-e5bbcf18666d",
            "name": "alvinmach",
            "img_url": "Picture",
            "description": "Hoping this works",
            "hours": "5:00 AM - 5:00 PM",
            "location": "Los Angeles",
        },
        {
            "uuid": "4b3875cc-c689-4db0-bd86-e9319319de17",
            "name": "machalvin",
            "img_url": "Picture2",
            "description": "Hoping this works as well",
            "hours": "6:00 AM - 6:00 PM",
            "location": "Los Angeles",
        },
    ]

    response = client.get("/businesses/location/{location}")
    app.dependency_overrides = {}
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
