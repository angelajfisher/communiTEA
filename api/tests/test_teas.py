from fastapi.testclient import TestClient
from queries.teas import TeaRepository
from main import app


client = TestClient(app)


class EmptyTeaRepository:
    def get_all(self):
        return []


class TestTeaRepository:
    def get_all(self):
        return [
            {
                "name": "Green Tea",
                "description": "This tea is green.",
                "img_url": "image.testing/green-tea.png",
                "published": True,
                "uuid": "8cd7bcaa-27b1-4a95-980b-cd9caf668176",
            },
            {
                "name": "Black Tea",
                "description": "This tea is black.",
                "img_url": "image.testing/black-tea.png",
                "published": True,
                "uuid": "e03a0956-2aa9-4350-8cd0-1dd7af7cbae7",
            },
            {
                "name": "White Tea",
                "description": "This tea is white.",
                "img_url": "image.testing/white-tea.png",
                "published": True,
                "uuid": "080325d5-6688-457a-bb8a-0d873634b207",
            },
        ]


def test_get_all_teas_empty():
    app.dependency_overrides[TeaRepository] = EmptyTeaRepository

    response = client.get("/teas")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_get_all_teas():
    app.dependency_overrides[TeaRepository] = TestTeaRepository
    expected = [
        {
            "name": "Green Tea",
            "description": "This tea is green.",
            "img_url": "image.testing/green-tea.png",
            "published": True,
            "uuid": "8cd7bcaa-27b1-4a95-980b-cd9caf668176",
        },
        {
            "name": "Black Tea",
            "description": "This tea is black.",
            "img_url": "image.testing/black-tea.png",
            "published": True,
            "uuid": "e03a0956-2aa9-4350-8cd0-1dd7af7cbae7",
        },
        {
            "name": "White Tea",
            "description": "This tea is white.",
            "img_url": "image.testing/white-tea.png",
            "published": True,
            "uuid": "080325d5-6688-457a-bb8a-0d873634b207",
        },
    ]

    response = client.get("/teas")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
