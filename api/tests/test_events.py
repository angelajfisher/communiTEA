from queries.events import EventRepository
from fastapi.testclient import TestClient
from main import app
from fastapi import status

client = TestClient(app)


class MockGetAllEventQueries:
    def get_all(self):
        return [
            {
                "title": "Tea Lovers Unit Test Event",
                "location": "West Hollywood",
                "date": "2023-10-30",
                "time": "05:05:05",
                "description": "Join us for our Tea Lovers' Event.",
                "img_url": "image url test",
                "uuid": "b2e46363-f7e5-4da8-9601-e6900cdb4556",
                "creator": "29f96c3e-41b3-4fc4-b44e-036ad466067f",
            },
            {
                "title": "Tea Lovers Unit Test Event 2",
                "location": "West Hollywood",
                "date": "2023-11-24",
                "time": "05:05:05",
                "description": "Join us for our Tea Lovers' Event.",
                "img_url": "image url test",
                "uuid": "b2e46363-f7e5-4da8-9601-e6900cdb4557",
                "creator": "29f96c3e-41b3-4fc4-b44e-036ad466067f",
            },
        ]


def test_get_all_events():
    app.dependency_overrides[EventRepository] = MockGetAllEventQueries
    expected = [
        {
            "title": "Tea Lovers Unit Test Event",
            "location": "West Hollywood",
            "date": "2023-10-30",
            "time": "05:05:05",
            "description": "Join us for our Tea Lovers' Event.",
            "img_url": "image url test",
            "uuid": "b2e46363-f7e5-4da8-9601-e6900cdb4556",
            "creator": "29f96c3e-41b3-4fc4-b44e-036ad466067f",
        },
        {
            "title": "Tea Lovers Unit Test Event 2",
            "location": "West Hollywood",
            "date": "2023-11-24",
            "time": "05:05:05",
            "description": "Join us for our Tea Lovers' Event.",
            "img_url": "image url test",
            "uuid": "b2e46363-f7e5-4da8-9601-e6900cdb4557",
            "creator": "29f96c3e-41b3-4fc4-b44e-036ad466067f",
        },
    ]

    response = client.get("/events")

    app.dependency_overrides = {}

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected
