import os
import bcrypt
from calendar import timegm
from datetime import datetime, timedelta
from fastapi import Cookie
from jose import JWTError, jwt
from jose.constants import ALGORITHMS
from typing import Annotated, Optional
from models.users import UserModel, UserOut
from pydantic import BaseModel

ALGORITHM = ALGORITHMS.HS256
SIGNING_KEY = os.environ.get("SIGNING_KEY")
if not SIGNING_KEY:
    raise ValueError("SIGNING_KEY environment variable not set")


class JWTUserData(UserModel):
    uuid: str
    role: int


class JWTPayload(BaseModel):
    user: JWTUserData
    sub: str
    exp: int


async def decode_jwt(token: str) -> Optional[JWTPayload]:
    """
    Helper function to decode the JWT from a token string
    """
    try:
        payload = jwt.decode(token, SIGNING_KEY, algorithms=[ALGORITHM])
        return JWTPayload(**payload)
    except (JWTError, AttributeError) as e:
        print(e)
    return None


async def try_get_jwt_user_data(
    access_token: Annotated[str | None, Cookie()] = None,
) -> Optional[JWTUserData]:
    """
    This function can be dependency injected into a route

    It checks the JWT token from the cookie and attempts to get the user
    from the payload of the JWT
    """
    if not access_token:
        return

    payload = await decode_jwt(access_token)
    if not payload:
        return

    return payload.user.dict()


def verify_password(plain_password, hashed_password) -> bool:
    """
    This verifies the user's password, by hashing the plain
    password and then comparing it to the hashed password
    from the database
    """
    return bcrypt.checkpw(
        plain_password.encode("utf-8"), hashed_password.encode("utf-8")
    )


def hash_password(plain_password) -> str:
    """
    Helper function that hashes a password
    """
    return bcrypt.hashpw(
        plain_password.encode("utf-8"), bcrypt.gensalt()
    ).decode()


def generate_jwt(user: UserOut) -> str:
    """
    Generates a new JWT token using the user's information

    We store the user as a JWTUserData converted to a dictionary
    in the payload of the JWT
    """
    exp = timegm((datetime.utcnow() + timedelta(hours=1)).utctimetuple())
    user_data = user.model_dump()
    user_data["uuid"] = str(user.uuid)
    jwt_data = JWTPayload(
        exp=exp,
        sub=user.username,
        user=JWTUserData(**user_data),
    )
    encoded_jwt = jwt.encode(
        jwt_data.model_dump(), SIGNING_KEY, algorithm=ALGORITHMS.HS256
    )
    return encoded_jwt
