from pydantic import UUID4, BaseModel
from fastapi import APIRouter, Depends, Response
from typing import List
from models.default import Error
from models.reviews import ReviewIn, ReviewOut
from queries.reviews import ReviewRepository
from queries.businesses import BusinessRepository
from utils.authentication import try_get_jwt_user_data

router = APIRouter()


class DeleteReviewRequest(BaseModel):
    user_id: int


class DeleteReviewResponse(BaseModel):
    success: bool
    message: str


@router.post(
    "/businesses/{business_uuid}/reviews/",
    response_model=ReviewOut | Error,
    tags=["businesses"],
)
def add_business_review(
    business_uuid: UUID4,
    review: ReviewIn,
    response: Response,
    repo: ReviewRepository = Depends(),
    business_repo: BusinessRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> ReviewOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    business_owner_role = business_repo.get_owner_role(business_uuid)
    if business_owner_role != 1:
        response.status_code = 400
        message = "Only business owners can be reviewed."
        return {"message": message}

    if repo.has_reviewed(user_data["uuid"], business_uuid):
        response.status_code = 400
        message = "You have already reviewed this business."
        return {"message": message}

    try:
        return repo.create(review, user_data["uuid"], business_uuid)
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Could not create review."
        return {"message": message}


@router.get(
    "/businesses/{business_uuid}/reviews/",
    response_model=List[ReviewOut] | Error,
    tags=["businesses"],
)
def get_business_reviews(
    business_uuid: UUID4,
    response: Response,
    repo: ReviewRepository = Depends(),
    business_repo: BusinessRepository = Depends(),
) -> List[ReviewOut]:
    try:
        business_owner_role = business_repo.get_owner_role(business_uuid)
        if business_owner_role != 1:
            response.status_code = 400
            message = "This user is not a business owner."
            return {"message": message}

        return repo.get_business_reviews(business_uuid)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not fetch reviews."}


@router.put(
    "/reviews/{review_uuid}/",
    response_model=ReviewOut | Error,
)
def update_business_review(
    review_uuid: UUID4,
    review: ReviewIn,
    response: Response,
    repo: ReviewRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> ReviewOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if not repo.is_authorized_to_update_review(
            review_uuid, user_data["uuid"]
        ):
            response.status_code = 403
            message = "You do not have permission to update this review."
            return {"message": message}
        return repo.update(review_uuid, review)
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Could not update review."
        return {"message": message}


@router.delete("/reviews/{review_uuid}/", response_model=bool | Error)
def delete_business_review(
    review_uuid: UUID4,
    response: Response,
    repo: ReviewRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
):
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if not repo.is_authorized_to_delete_review(
            review_uuid, user_data["uuid"]
        ):
            response.status_code = 403
            message = "You do not have permission to delete this review."
            return {"message": message}
        success = repo.delete(review_uuid)
        return {"success": success, "message": "Review deleted successfully."}
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not delete the review."}
