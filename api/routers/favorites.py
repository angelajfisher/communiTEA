from fastapi import APIRouter, Depends, Response
from models.default import Error
from models.favorites import FavIn, FavOut
from queries.favorites import FavRepository
from utils.authentication import try_get_jwt_user_data
from pydantic import UUID4
from typing import List


router = APIRouter()


@router.post("/favorites/", response_model=FavOut | Error)
def create_favorite(
    fav: FavIn,
    response: Response,
    repo: FavRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> FavOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if repo.check_existence(fav):
            response.status_code = 400
            return {"message": "This tea is already on this user's list."}

        new_fav = repo.create(fav)
        if new_fav is not None:
            return new_fav

        response.status_code = 404
        return {"message": "Could not add to favorites. Check user & tea IDs."}

    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not add to favorites."}


@router.get(
    "/users/{user_uuid}/favorites/", response_model=List[FavOut] | Error
)
def get_user_favorites(
    user_uuid: UUID4,
    response: Response,
    repo: FavRepository = Depends(),
) -> List[FavOut]:

    try:
        return repo.get_user_favs(user_uuid)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get list of user's favorite teas."}


@router.delete("/favorites/{fav_uuid}/", response_model=bool | Error)
def delete_favorite(
    fav_uuid: UUID4,
    response: Response,
    repo: FavRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> bool:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        fav = repo.get(fav_uuid)

        if fav is None:
            return True

        if str(user_data["uuid"]) != str(fav.user_uuid):
            response.status_code = 403
            return {"message": "This favorite is unassociated with this user."}

        repo.delete(fav_uuid)
        return True

    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not delete favorite."}
