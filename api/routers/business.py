from fastapi import APIRouter, Depends, Response
from models.default import Error
from models.businesses import (
    BusinessOut,
    SimpleBusinessResponse,
)
from models.users import UserIn
from queries.businesses import BusinessRepository
from pydantic import UUID4
from utils.authentication import try_get_jwt_user_data
from typing import List

router = APIRouter(prefix="/businesses")


@router.get("/{user_uuid}/", response_model=BusinessOut | Error)
def get_business_info(
    user_uuid: UUID4,
    response: Response,
    repo: BusinessRepository = Depends(),
) -> BusinessOut:
    try:
        user = repo.get_business_profile(user_uuid)
        return user
    except Exception as e:
        print(e)
        response.status_code = 404
        return {"message": "Could not find business user."}


@router.put("/{user_uuid}/", response_model=BusinessOut | Error)
def update_business_profile(
    user_uuid: UUID4,
    updated_profile: UserIn,
    response: Response,
    repo: BusinessRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> BusinessOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    business = repo.update_business_profile(
        user_uuid, updated_profile, user_data["uuid"]
    )
    if business is None:
        response.status_code = 404
        return {"message": "Could not update the business profile"}
    return business


@router.get(
    "/location/{location}/",
    response_model=List[SimpleBusinessResponse] | Error,
)
def get_businesses_by_location(
    location: str,
    response: Response,
    repo: BusinessRepository = Depends(),
) -> List[SimpleBusinessResponse]:
    try:
        businesses = repo.get_businesses_by_location(location)
        return businesses
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Could not retrieve businesses by location."
        return [{"message": message}]
