from fastapi import (
    Depends,
    Response,
    APIRouter,
    Request,
)
from models.default import Error
from models.users import UserIn, UserOut, LoginForm
from queries.users import UserRepository
from utils.authentication import (
    try_get_jwt_user_data,
    generate_jwt,
    verify_password,
)

router = APIRouter()


@router.get("/authenticate/", response_model=UserOut | None)
async def get_token(
    request: Request,
    user: UserIn = Depends(try_get_jwt_user_data),
) -> UserOut | None:
    if user and "access_token" in request.cookies:
        return user
    return None


@router.post("/signin/", response_model=UserOut | Error)
async def signin(
    user_request: LoginForm,
    request: Request,
    response: Response,
    repo: UserRepository = Depends(),
) -> UserOut:
    user = repo.get_with_password(user_request.username)
    if not user:
        response.status_code = 401
        return {"message": "Incorrect username or password."}

    if not verify_password(user_request.password, user.hashed_password):
        response.status_code = 401
        return {"message": "Incorrect username or password."}

    token = generate_jwt(UserOut(**user.model_dump()))

    response.set_cookie(
        key="access_token",
        value=token,
        httponly=True,
        samesite="none",
        secure=True,
    )

    return UserOut(**user.model_dump())


@router.delete("/signout/")
async def signout(
    request: Request,
    response: Response,
):
    response.delete_cookie(
        key="access_token", httponly=True, samesite="lax", secure=True
    )

    return True
