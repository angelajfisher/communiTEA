from fastapi import (
    Depends,
    Response,
    APIRouter,
    Request,
)
from models.default import Error
from models.users import UserIn, UserOut
from queries.users import UserRepository
from pydantic import UUID4
from utils.authentication import (
    try_get_jwt_user_data,
    hash_password,
    generate_jwt,
)

router = APIRouter(prefix="/users")


@router.post("/", response_model=UserOut | Error)
async def create_user(
    user: UserIn,
    request: Request,
    response: Response,
    repo: UserRepository = Depends(),
):
    hashed_password = hash_password(user.password)
    try:
        if repo.check_existence(user.username):
            response.status_code = 400
            return {
                "message": "Could not create user: that username is taken."
            }
        new_user = repo.create(user, hashed_password)
        token = generate_jwt(new_user)
        secure = (
            True if request.headers.get("origin") == "localhost" else False
        )
        response.set_cookie(
            key="access_token",
            value=token,
            httponly=True,
            samesite="lax",
            secure=secure,
        )
        return new_user
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not create user."}


@router.get("/{user_uuid}/", response_model=UserOut | Error)
def get_user(
    user_uuid: UUID4,
    response: Response,
    repo: UserRepository = Depends(),
) -> UserOut:
    try:
        user = repo.get(user_uuid)
        if user is None:
            response.status_code = 404
            return {"message": "No user with that ID was found."}
        return user
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get user."}


@router.delete("/", response_model=bool | Error)
def delete_user(
    response: Response,
    repo: UserRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> bool:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        repo.delete(user_data["uuid"])
        response.delete_cookie(
            key="access_token", httponly=True, samesite="lax", secure=True
        )
        return True
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not delete user."}


@router.put("/{user_uuid}", response_model=UserOut | Error)
def update_user(
    user_uuid: UUID4,
    user_edit: UserIn,
    response: Response,
    repo: UserRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> UserOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("uuid") != user_uuid:
            response.status_code = 403
            message = "Could not update user: user does not have permission."
            return {"message": message}

        if user_edit.role > 2 or user_edit.role < 0:
            response.status_code = 400
            return {
                "message": "Could not update user: invalid role specified."
            }

        if user_edit.role == 2 and user_data.get("role") != 2:
            response.status_code = 403
            message = "Could not update user: unauthorized role specified."
            return {"message": message}

        new_password_hash = hash_password(user_edit.password)
        return repo.update(user_edit, user_uuid, new_password_hash)

    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not update user."}


@router.put("/{user_uuid}/promote/", response_model=UserOut | Error)
async def promote_to_admin(
    user_uuid: UUID4,
    response: Response,
    repo: UserRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> UserOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") != 2:
            response.status_code = 403
            message = "Could not promote user: user does not have permission."
            return {"message": message}

        user = repo.get(user_uuid)
        if user is None:
            response.status_code = 404
            return {"message": "No user with that ID was found."}

        if user.role == 2:
            return user

        user = repo.promote(user_uuid)
        return user
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not promote user."}
