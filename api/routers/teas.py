from fastapi import APIRouter, Depends, Response
from models.default import Error
from models.businesses import BusinessOut, MiniBusiness
from models.teas import TeaIn, TeaOut, DraftOut, TeaOutWithLikes
from queries.teas import TeaRepository
from queries.businesses import BusinessRepository
from utils.authentication import try_get_jwt_user_data
from pydantic import UUID4
from typing import List


router = APIRouter(prefix="/teas")


@router.post("/", response_model=TeaOut | Error)
def create_tea(
    tea: TeaIn,
    response: Response,
    repo: TeaRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> TeaOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") == 0:
            response.status_code = 403
            message = "Could not create tea: user does not have permission."
            return {"message": message}

        if repo.check_existence(tea.name):
            message = "Could not create tea: teas must have a unique name."
            response.status_code = 400
            return {"message": message}

        if user_data.get("role") == 1:
            tea.published = False

        return repo.create(tea)

    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Could not create tea."
        return {"message": message}


@router.get("/popular/", response_model=List[TeaOutWithLikes] | Error)
def get_popular_teas(
    response: Response, repo: TeaRepository = Depends()
) -> List[TeaOutWithLikes]:

    try:
        return repo.get_popular()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get list of popular teas."}


@router.get("/unpublished/", response_model=List[TeaOut] | Error)
def get_unpublished_teas(
    response: Response,
    repo: TeaRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> List[TeaOut]:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") != 2:
            response.status_code = 403
            message = "Could not get unpublished teas: user is not authorized."
            return {"message": message}
        return repo.get_unpublished()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get unpublished teas."}


@router.get("/submissions/", response_model=List[DraftOut] | Error)
def get_tea_submissions(
    response: Response,
    repo: TeaRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> List[DraftOut]:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") != 2:
            response.status_code = 403
            message = "Could not get tea submissions: user is not authorized."
            return {"message": message}
        return repo.get_drafts()
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get unpublished teas."}


@router.get("/{tea_uuid}/", response_model=TeaOut | Error)
def get_tea(
    tea_uuid: UUID4, response: Response, repo: TeaRepository = Depends()
) -> TeaOut:

    try:
        tea = repo.get_one(tea_uuid)
        if tea is None:
            response.status_code = 404
            return {"message": "No tea with that ID was found."}
        return tea
    except Exception as e:
        print(e)
        return {"message": "Could not edit tea."}


@router.get("/", response_model=List[TeaOut] | Error)
def get_all_teas(
    response: Response, repo: TeaRepository = Depends()
) -> List[TeaOut]:

    try:
        return repo.get_all()
    except Exception as e:
        print(e)
        return {"message": "Could not get teas."}
        response.status_code = 400


@router.put("/{tea_uuid}/publish/", response_model=TeaOut | Error)
def publish_tea(
    tea_uuid: UUID4,
    response: Response,
    repo: TeaRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> TeaOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") != 2:
            response.status_code = 403
            message = "Could not publish tea: user does not have permission."
            return {"message": message}

        tea = repo.get_one(tea_uuid)
        if tea is None:
            response.status_code = 404
            return {"message": "No tea with that ID was found."}

        return repo.publish(tea_uuid)

    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not update tea."}


@router.put("/{tea_uuid}/", response_model=TeaOut | Error)
def update_tea(
    tea_uuid: UUID4,
    tea_edit: TeaIn,
    response: Response,
    repo: TeaRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> TeaOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") == 0:
            response.status_code = 403
            message = "Could not update tea: user does not have permission."
            return {"message": message}

        tea = repo.get_one(tea_uuid)
        if tea is None:
            response.status_code = 404
            return {"message": "No tea with that ID was found."}

        if user_data.get("role") == 2:
            tea = repo.update(tea_uuid, tea_edit)
            return tea

        tea = repo.create_draft(tea_uuid, tea_edit)
        return tea

    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not update tea."}


@router.delete("/{tea_uuid}/", response_model=bool | Error)
def delete_tea(
    tea_uuid: UUID4,
    response: Response,
    repo: TeaRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
):
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") != 2:
            response.status_code = 403
            message = "Could not delete tea: user does not have permission."
            return {"message": message}

        return repo.delete(tea_uuid)

    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not delete tea."}


@router.delete("/submissions/{draft_uuid}/", response_model=bool | Error)
def delete_draft(
    draft_uuid: UUID4,
    response: Response,
    repo: TeaRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
):
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        if user_data.get("role") != 2:
            response.status_code = 403
            message = "Could not delete draft: user does not have permission."
            return {"message": message}

        return repo.delete_draft(draft_uuid)

    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not delete draft."}


@router.get(
    "/{tea_uuid}/businesses/",
    response_model=List[MiniBusiness] | Error,
    tags=["businesses"],
)
def get_nearby_businesses_with_tea(
    tea_uuid: UUID4,
    response: Response,
    repo: BusinessRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> List[BusinessOut]:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        return repo.get_businesses(tea_uuid, user_data["location"])
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get list of businesses with that tea."}
