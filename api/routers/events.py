from fastapi import (
    Depends,
    Response,
    APIRouter,
)
from models.default import Error
from models.events import (
    EventOut,
    EventIn,
)
from queries.events import EventRepository
from uuid import UUID
from utils.authentication import try_get_jwt_user_data
from typing import List

router = APIRouter(prefix="/events")


@router.post("/", response_model=EventOut | Error)
def create_event(
    event: EventIn,
    response: Response,
    repo: EventRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> EventOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        return repo.create(event, user_data["uuid"])
    except Exception as e:
        print(e)
        response.status_code = 400
        message = "Could not create event."
        return {"message": message}


@router.get("/nearby/", response_model=List[EventOut] | Error)
def get_nearby_events(
    response: Response,
    repo: EventRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> List[EventOut]:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    try:
        current_user_location = user_data.get("location")
        return repo.get_nearby(current_user_location)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get nearby events."}


@router.get("/{event_uuid}/", response_model=EventOut | Error)
def get_one_event(
    event_uuid: UUID,
    response: Response,
    repo: EventRepository = Depends(),
) -> EventOut:
    event = repo.get_one(event_uuid)
    if event is None:
        response.status_code = 404
        return {"message": "That event does not exist"}
    return event


@router.put("/{event_uuid}/", response_model=EventOut | Error)
def update_event(
    event_uuid: UUID,
    updated_event: EventIn,
    response: Response,
    repo: EventRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> EventOut:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    creator_uuid = user_data["uuid"]
    event = repo.update_event(event_uuid, updated_event, creator_uuid)
    if event is None:
        response.status_code = 404
        return {"message": "Could not update the event"}
    return event


@router.get("/", response_model=List[EventOut] | Error)
def get_all_events(
    response: Response,
    repo: EventRepository = Depends(),
) -> List[EventOut]:

    try:
        return repo.get_all()
    except Exception as e:
        print(e)
        response.status_code = 404
        return {"message": "Could not get all events"}


@router.delete("/{event_uuid}/", response_model=bool)
def delete_event(
    event_uuid: UUID,
    response: Response,
    repo: EventRepository = Depends(),
    user_data: dict = Depends(try_get_jwt_user_data),
) -> bool:
    if not user_data:
        response.status_code = 401
        return {"message": "Unauthenticated: please log in to continue."}

    creator_uuid = user_data["uuid"]
    return repo.delete(event_uuid, creator_uuid)
