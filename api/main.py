import ssl
import os
from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from fastapi.middleware.cors import CORSMiddleware

from routers import (
    authentication,
    users,
    teas,
    events,
    business,
    reviews,
    favorites,
)

root_path = os.environ["ROOT_PATH"]

if os.environ["DEV_MODE"] != "true":
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    ssl_context.load_cert_chain("ssl/cert.pem", keyfile="ssl/key.pem")

app = FastAPI(root_path=root_path)
app.include_router(authentication.router, tags=["authentication"])
app.include_router(users.router, tags=["users"])
app.include_router(teas.router, tags=["teas"])
app.include_router(events.router, tags=["events"])
app.include_router(business.router, tags=["businesses"])
app.include_router(reviews.router, tags=["reviews"])
app.include_router(favorites.router, tags=["favorites"])


@app.get("/")
def root_redirect_to_docs():
    return RedirectResponse(f"{root_path}/docs")


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ["CORS_HOST"]],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
