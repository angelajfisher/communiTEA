steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE drafts (
            uuid UUID PRIMARY KEY,
            tea_uuid UUID NOT NULL REFERENCES teas(uuid) ON DELETE CASCADE,
            img_url VARCHAR(500),
            description TEXT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE drafts;
        """,
    ],
]
