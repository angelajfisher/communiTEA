steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            uuid UUID PRIMARY KEY,
            username VARCHAR(200) NOT NULL UNIQUE,
            role INTEGER NOT NULL DEFAULT 0,
            img_url VARCHAR(1000),
            display_name VARCHAR(200) NOT NULL,
            description TEXT,
            hours VARCHAR(300),
            location VARCHAR(300) NOT NULL,
            hashed_password VARCHAR(500) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE teas (
            uuid UUID PRIMARY KEY,
            name VARCHAR(200) NOT NULL UNIQUE,
            description TEXT NOT NULL,
            img_url VARCHAR(500),
            published BOOLEAN NOT NULL DEFAULT FALSE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE teas;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE favorites (
            uuid UUID PRIMARY KEY,
            user_uuid UUID NOT NULL REFERENCES users(uuid) ON DELETE CASCADE,
            tea_uuid UUID NOT NULL REFERENCES teas(uuid) ON DELETE CASCADE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE favorites;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE reviews (
            uuid UUID PRIMARY KEY,
            rating INTEGER NOT NULL,
            comment TEXT,
            author UUID NOT NULL REFERENCES users(uuid) ON DELETE CASCADE,
            business UUID NOT NULL REFERENCES users(uuid) ON DELETE CASCADE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE reviews;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE events (
            uuid UUID PRIMARY KEY,
            creator UUID NOT NULL REFERENCES users(uuid) ON DELETE CASCADE,
            title VARCHAR(300) NOT NULL,
            location VARCHAR(500) NOT NULL,
            date DATE NOT NULL,
            time TIME NOT NULL,
            description TEXT NOT NULL,
            img_url VARCHAR(500)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE events;
        """,
    ],
]
