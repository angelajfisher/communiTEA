from models.users import UserIn, UserOut, UserOutWithPassword
from pydantic import UUID4
import uuid
from queries.pool import pool


class UserRepository:
    def get(self, uuid: UUID4) -> UserOut:
        """
        Retrieves a specific user from the database from their UUID.
        Requires a valid UUID.
        Returns a UserOut instance with the data from the relevant row.
        If none found, returns None.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM users
                        WHERE uuid = %s
                        """,
                        [uuid],
                    )

                    record = result.fetchone()
                    if record is None:
                        return None
                    return UserOut(
                        uuid=record[0],
                        username=record[1],
                        role=record[2],
                        img_url=record[3],
                        display_name=record[4],
                        description=record[5],
                        hours=record[6],
                        location=record[7],
                    )
        except Exception as e:
            print(e)

    def get_with_password(self, username: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )

                    record = result.fetchone()
                    if not record:
                        return None
                    return UserOutWithPassword(
                        uuid=str(record[0]),
                        username=record[1],
                        role=record[2],
                        img_url=record[3],
                        display_name=record[4],
                        description=record[5],
                        hours=record[6],
                        location=record[7],
                        hashed_password=record[8],
                    )
        except Exception as e:
            print(e)

    def create(self, user: UserIn, hashed_password: str) -> UserOut:
        new_uuid = uuid.uuid4()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (
                            uuid,
                            username,
                            role,
                            img_url,
                            display_name,
                            description,
                            hours,
                            location,
                            hashed_password
                            )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [
                            new_uuid,
                            user.username,
                            user.role,
                            user.img_url,
                            user.display_name,
                            user.description,
                            user.hours,
                            user.location,
                            hashed_password,
                        ],
                    )
                    record = result.fetchone()
                    return UserOut(
                        uuid=str(record[0]),
                        username=record[1],
                        role=record[2],
                        img_url=record[3],
                        display_name=record[4],
                        description=record[5],
                        hours=record[6],
                        location=record[7],
                    )
        except Exception as e:
            print(e)

    def update(
        self, user_edit: UserIn, user_uuid: UUID4, hashed_password: str
    ) -> UserOut:
        """
        Changes a specified row in the users database with the provided info.
        Requires a valid UUID.
        Returns a UserOut instance with the updated data from the db row.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE users
                        SET role = %s
                            , img_url = %s
                            , display_name = %s
                            , description = %s
                            , hours = %s
                            , location = %s
                            , hashed_password = %s
                        WHERE uuid = %s
                        RETURNING *;
                        """,
                        [
                            user_edit.role,
                            user_edit.img_url,
                            user_edit.display_name,
                            user_edit.description,
                            user_edit.hours,
                            user_edit.location,
                            hashed_password,
                            user_uuid,
                        ],
                    )
                    record = result.fetchone()
                    return UserOut(
                        uuid=str(record[0]),
                        username=record[1],
                        role=record[2],
                        img_url=record[3],
                        display_name=record[4],
                        description=record[5],
                        hours=record[6],
                        location=record[7],
                    )
        except Exception as e:
            print(e)

    def promote(self, user_uuid: UUID4) -> UserOut:
        """
        Changes the role of specified row in the users database to 2 (admin).
        Requires a valid UUID.
        Returns a UserOut instance with the updated data from the db row.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE users
                        SET role = 2
                        WHERE uuid = %s
                        RETURNING *;
                        """,
                        [
                            user_uuid,
                        ],
                    )
                    record = result.fetchone()
                    return UserOut(
                        uuid=str(record[0]),
                        username=record[1],
                        role=record[2],
                        img_url=record[3],
                        display_name=record[4],
                        description=record[5],
                        hours=record[6],
                        location=record[7],
                    )
        except Exception as e:
            print(e)

    def delete(self, user_uuid: UUID4) -> bool:
        """
        Removes a row from the users table corresponding to the given user ID.
        Requires a valid UUID.
        Returns True upon successful deletion.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM users
                        WHERE uuid = %s
                        """,
                        [user_uuid],
                    )
                    return True
        except Exception as e:
            print(e)

    def check_existence(self, username: str) -> bool:
        """
        Checks if the given user is already in the database.
        Requires a username (str).
        Returns True if present and False if not present.
        """
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT username FROM users
                    WHERE username = %s
                    """,
                    [username],
                )
                if result.fetchone() is None:
                    return False
                return True
