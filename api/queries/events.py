from models.events import EventIn, EventOut
from pydantic import UUID4
from typing import Optional
import uuid
from queries.pool import pool
from uuid import UUID
from typing import List
from fastapi import HTTPException


class EventRepository:
    def create(self, event: EventIn, creator: UUID4) -> EventOut:
        new_uuid = uuid.uuid4()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO events(
                            uuid,
                            creator,
                            title,
                            location,
                            date,
                            time,
                            description,
                            img_url
                        )
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING uuid
                        """,
                        [
                            new_uuid,
                            creator,
                            event.title,
                            event.location,
                            event.date,
                            event.time,
                            event.description,
                            event.img_url,
                        ],
                    )
                    old_data = event.dict()
                    old_data["uuid"] = new_uuid
                    old_data["creator"] = creator
                    return EventOut(**old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not create an event."}

    def get_one(self, event_uuid: UUID) -> Optional[EventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            uuid,
                            creator,
                            title,
                            location,
                            date,
                            time,
                            description,
                            img_url
                        FROM events
                        WHERE uuid = %s
                        """,
                        [event_uuid],
                    )
                    event = result.fetchone()
                    if event is None:
                        return None
                    return EventOut(
                        uuid=event[0],
                        creator=event[1],
                        title=event[2],
                        location=event[3],
                        date=event[4],
                        time=event[5],
                        description=event[6],
                        img_url=event[7],
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not get that event"}

    def update_event(
        self, event_uuid: UUID, event: EventIn, creator_uuid: UUID4
    ) -> Optional[EventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE events
                        SET title = %s,
                            location = %s,
                            date = %s,
                            time = %s,
                            description = %s,
                            img_url = %s
                        WHERE uuid = %s AND creator = %s
                        RETURNING uuid,
                            creator,
                            title,
                            location,
                            date,
                            time,
                            description,
                            img_url
                        """,
                        [
                            event.title,
                            event.location,
                            event.date,
                            event.time,
                            event.description,
                            event.img_url,
                            event_uuid,
                            creator_uuid,
                        ],
                    )
                    event = db.fetchone()
                    if event is None:
                        return None
                    return EventOut(
                        uuid=event[0],
                        creator=event[1],
                        title=event[2],
                        location=event[3],
                        date=event[4],
                        time=event[5],
                        description=event[6],
                        img_url=event[7],
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not update the event"}

    def get_all(self) -> List[EventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM events
                        """
                    )
                    records = db.fetchall()
                    events = []
                    for record in records:
                        event = EventOut(
                            uuid=record[0],
                            creator=record[1],
                            title=record[2],
                            location=record[3],
                            date=record[4],
                            time=record[5],
                            description=record[6],
                            img_url=record[7],
                        )
                        events.append(event)
                    return events
        except Exception as e:
            print(e)
            return {"message": "Could not get all events"}

    def get_nearby(self, location: str) -> List[EventOut]:
        """
        Retrieves a list of events that match a given location.
        Requires a string.
        Returns a list of EventOut instances with the relevant data.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM events
                        WHERE location = %s
                        """,
                        [location],
                    )
                    records = db.fetchall()
                    events = []
                    for record in records:
                        event = EventOut(
                            uuid=record[0],
                            creator=record[1],
                            title=record[2],
                            location=record[3],
                            date=record[4],
                            time=record[5],
                            description=record[6],
                            img_url=record[7],
                        )
                        events.append(event)
                    return events
        except Exception as e:
            print(e)

    def delete(self, event_uuid: UUID, creator_uuid: UUID4) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM events
                        WHERE uuid = %s AND creator = %s
                        """,
                        [event_uuid, creator_uuid],
                    )
                    if db.rowcount == 0:
                        raise HTTPException(
                            status_code=404,
                            detail="Event not found or unauthorized to delete",
                        )
                    return True
        except HTTPException:
            raise
        except Exception as e:
            print(e)
            return False
