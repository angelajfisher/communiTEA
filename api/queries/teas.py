from models.teas import TeaIn, TeaOut, TeaOutWithLikes, DraftOut
from pydantic import UUID4
from typing import List
from queries.pool import pool
import uuid


class TeaRepository:
    def create(self, tea: TeaIn) -> TeaOut:
        """
        Adds a new tea to the database.
        Requires a valid TeaIn instance.
        Returns a TeaOut instance with the newly created data.
        """
        new_uuid = uuid.uuid4()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO teas(
                            uuid,
                            name,
                            description,
                            img_url,
                            published
                        )
                        VALUES (%s, %s, %s, %s, %s)
                        RETURNING uuid
                        """,
                        [
                            new_uuid,
                            tea.name,
                            tea.description,
                            tea.img_url,
                            tea.published,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = tea.dict()
                    return TeaOut(uuid=id, **old_data)
        except Exception as e:
            print(e)

    def get_one(self, tea_id: UUID4) -> TeaOut:
        """
        Retrieves a specific tea from the database from its UUID.
        Requires a valid UUID.
        Returns a TeaOut instance with the data from the relevant row.
        If none found, returns None.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT * FROM teas
                        WHERE uuid = (%s)
                        """,
                        [tea_id],
                    )
                    tea = result.fetchone()
                    if tea is None:
                        return None
                    return self.record_to_tea_out(tea)
        except Exception as e:
            print(e)

    def get_all(self) -> List[TeaOut]:
        """
        Retrieves every tea in the database with Published set to True.
        Returns a list of TeaOuts.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT * FROM teas
                        WHERE published = True
                        """
                    )

                    tea_list = []
                    for record in result:
                        tea = self.record_to_tea_out(record)
                        tea_list.append(tea)
                    return tea_list

        except Exception as e:
            print(e)
            return None

    def get_unpublished(self) -> List[TeaOut]:
        """
        Retrieves every tea in the database with Published set to False.
        Returns a list of TeaOuts.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT * FROM teas
                        WHERE published = False
                        """
                    )

                    tea_list = []
                    for record in result:
                        tea = self.record_to_tea_out(record)
                        tea_list.append(tea)
                    return tea_list
        except Exception as e:
            print(e)
            return None

    def get_popular(self) -> List[TeaOutWithLikes]:
        """
        Retrieves the five published teas with the most likes.
        Returns a list of TeaOutWithLikes.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT DISTINCT
                            t.uuid
                            , t.name
                            , t.description
                            , t.img_url
                            , f.likes
                        FROM(
                            SELECT tea_uuid, COUNT(user_uuid)
                                OVER(PARTITION BY tea_uuid) AS likes
                            FROM favorites
                        ) f JOIN teas t
                        ON f.tea_uuid = t.uuid
                        WHERE published = True
                        ORDER BY f.likes DESC
                        LIMIT 5;
                        """
                    )

                    tea_list = []
                    for record in result.fetchall():
                        tea = TeaOutWithLikes(
                            uuid=record[0],
                            name=record[1],
                            description=record[2],
                            img_url=record[3],
                            likes=record[4],
                            published=True,
                        )
                        tea_list.append(tea)
                    return tea_list

        except Exception as e:
            print(e)
            return None

    def update(self, tea_id: UUID4, tea: TeaIn) -> TeaOut:
        """
        Changes a specified row in the tea database with the provided info.
        Requires a valid UUID.
        Returns a TeaOut instance with the updated data from the db row.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE teas
                        SET description = %s
                            , img_url = %s
                        WHERE uuid = %s
                        RETURNING *
                        """,
                        [tea.description, tea.img_url, tea_id],
                    )
                    return self.record_to_tea_out(result.fetchone())
        except Exception as e:
            print(e)

    def publish(self, tea_id) -> TeaOut:
        """
        Changes a given tea's "published" property to True.
        Requires a valid tea UUID.
        Returns the updated data for the tea.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE teas
                        SET published = True
                        WHERE uuid = %s
                        RETURNING *
                        """,
                        [tea_id],
                    )

                    return self.record_to_tea_out(result.fetchone())
        except Exception as e:
            print(e)

    def delete(self, tea_id: UUID4) -> bool:
        """
        Removes a row from the teas table corresponding to the given tea ID.
        Requires a valid UUID.
        Returns True upon successful deletion.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM teas
                        WHERE uuid = %s
                        """,
                        [tea_id],
                    )
                    return True
        except Exception as e:
            print(e)

    def check_existence(self, name: str) -> bool:
        """
        Checks if the given tea is already in the database.
        Requires a tea name (str).
        Returns True if present and False if not present.
        """
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT name FROM teas
                    WHERE name = %s
                    """,
                    [name],
                )
                if result.fetchone() is None:
                    return False
                return True

    def create_draft(self, tea_id: UUID4, tea_edit: TeaIn) -> TeaOut:
        """
        Adds a new tea wiki page draft to the database for admin review.
        Requires a valid TeaIn instance and the UUID of an existing tea.
        Returns a TeaOut instance with the newly created data.
        """
        new_uuid = uuid.uuid4()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO drafts(
                            uuid,
                            tea_uuid,
                            img_url,
                            description
                        )
                        VALUES (%s, %s, %s, %s)
                        RETURNING *
                        """,
                        [
                            new_uuid,
                            tea_id,
                            tea_edit.img_url,
                            tea_edit.description,
                        ],
                    )
                    tea = result.fetchone()
                    return TeaOut(
                        uuid=new_uuid,
                        name=self.get_one(tea_id).name,
                        description=tea[3],
                        img_url=tea[2],
                    )
        except Exception as e:
            print(e)

    def get_drafts(self) -> List[DraftOut]:
        """
        Retrieves every draft in the database.
        Returns a list of DraftOuts.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT d.*, t.name
                        FROM drafts AS d
                        JOIN teas AS t
                            ON t.uuid = d.tea_uuid
                        """
                    )

                    draft_list = []
                    for record in result:
                        draft = DraftOut(
                            draft_uuid=record[0],
                            tea_uuid=record[1],
                            img_url=record[2],
                            description=record[3],
                            name=record[4],
                        )
                        draft_list.append(draft)
                    return draft_list
        except Exception as e:
            print(e)
            return None

    def delete_draft(self, draft_id: UUID4) -> bool:
        """
        Removes a row from the teas table corresponding to the given tea ID.
        Requires a valid UUID.
        Returns True upon successful deletion.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM drafts
                        WHERE uuid = %s
                        """,
                        [draft_id],
                    )
                    return True
        except Exception as e:
            print(e)

    def record_to_tea_out(self, record):
        try:
            return TeaOut(
                uuid=record[0],
                name=record[1],
                description=record[2],
                img_url=record[3],
                published=record[4],
            )
        except Exception as e:
            print(e)
