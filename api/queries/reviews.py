from models.reviews import ReviewIn, ReviewOut
from models.default import Error
from pydantic import UUID4
from typing import List
from queries.pool import pool
import uuid


class ReviewRepository:
    def create(
        self, review: ReviewIn, author_uuid: UUID4, business_uuid: UUID4
    ) -> ReviewOut:
        new_uuid = uuid.uuid4()
        """
        Adds a review to the database.
        Requires a valid ReviewIn instance.
        Returns a ReviewOut instance with the newly created data.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO reviews(
                            uuid,
                            rating,
                            comment,
                            author,
                            business
                        )
                        VALUES
                        (%s, %s, %s, %s, %s)
                        RETURNING uuid
                        """,
                        [
                            new_uuid,
                            review.rating,
                            review.comment,
                            author_uuid,
                            business_uuid,
                        ],
                    )
                    id = result.fetchone()[0]
                    return ReviewOut(
                        uuid=id,
                        rating=review.rating,
                        comment=review.comment,
                        business=business_uuid,
                        author=author_uuid,
                    )
        except Exception as e:
            print(e)
            return Error(message="Failed to create review")

    def has_reviewed(self, user_uuid: UUID4, business_uuid: UUID4) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT EXISTS (
                            SELECT 1
                            FROM reviews
                            WHERE author = %s
                            AND business = %s
                        )
                        """,
                        [user_uuid, business_uuid],
                    )
                    return result.fetchone()[0]
        except Exception as e:
            print(e)

    def get_business_reviews(self, business_uuid: UUID4) -> List[ReviewOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT uuid, rating, comment, business, author
                        FROM reviews
                        WHERE business = %s
                        """,
                        [business_uuid],
                    )
                    return [
                        ReviewOut(
                            uuid=row[0],
                            rating=row[1],
                            comment=row[2],
                            business=row[3],
                            author=row[4],
                        )
                        for row in db.fetchall()
                    ]
        except Exception as e:
            print(e)
            return Error(message="Failed to fetch reviews")

    def update(self, review_uuid: UUID4, review: ReviewIn) -> ReviewOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE reviews
                        SET rating = %s, comment = %s
                        WHERE uuid = %s
                        RETURNING uuid, business, author, rating, comment
                        """,
                        [review.rating, review.comment, review_uuid],
                    )
                    row = result.fetchone()
                    if row is not None:
                        return ReviewOut(
                            uuid=row[0],
                            business=row[1],
                            author=row[2],
                            rating=row[3],
                            comment=row[4],
                        )
        except Exception as e:
            print(e)
            return Error(message="Failed to update review")

    def is_authorized_to_update_review(
        self, review_uuid: UUID4, user_uuid: UUID4
    ) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT 1
                        FROM reviews
                        WHERE uuid = %s AND author = %s
                        """,
                        [review_uuid, user_uuid],
                    )
                    return bool(result.fetchone())
        except Exception as e:
            print(e)
            return False

    def delete(self, review_uuid: UUID4) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM reviews
                        WHERE uuid = %s
                        """,
                        [review_uuid],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def is_authorized_to_delete_review(
        self, review_uuid: UUID4, user_uuid: UUID4
    ) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT 1
                        FROM reviews
                        WHERE uuid = %s AND author = %s
                        """,
                        [review_uuid, user_uuid],
                    )
                    return bool(result.fetchone())
        except Exception as e:
            print(e)
            return False
