from models.businesses import (
    BusinessModel,
    BusinessOut,
    SimpleBusinessResponse,
    MiniBusiness,
)
from pydantic import UUID4
from queries.pool import pool
from queries.users import UserIn
from uuid import UUID
from typing import List


class BusinessRepository:
    def get_businesses(
        self, tea_id: UUID4, location: str
    ) -> List[MiniBusiness]:
        """
        Retrieves the businesses at the given location with the given tea
        on their offerings list.
        Requires a valid tea UUID and a valid location (str).
        Returns a list of matching BusinessOuts (uuid, display_name, img_url).
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            b.display_name
                            , b.img_url
                            , b.uuid
                        FROM favorites
                        JOIN users AS b
                            ON favorites.user_uuid = b.uuid
                        WHERE favorites.tea_uuid = %s
                            AND b.location = %s
                            AND b.role = 1;
                        """,
                        [tea_id, location],
                    )

                    business_list = []
                    for record in result.fetchall():
                        business = BusinessModel(
                            name=record[0],
                            img_url=record[1],
                            uuid=record[2],
                        )
                        business_list.append(business)
                    return business_list
        except Exception as e:
            print(e)

    def get_business_profile(self, uuid: UUID4) -> BusinessOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM users
                        WHERE uuid = %s
                        """,
                        [uuid],
                    )
                    business_user = result.fetchone()
                    business = BusinessOut(
                        uuid=business_user[0],
                        username=business_user[1],
                        role=business_user[2],
                        img_url=business_user[3],
                        name=business_user[4],
                        description=business_user[5],
                        hours=business_user[6],
                        location=business_user[7],
                    )
                    return business
        except Exception as e:
            print(e)
            return {"message": "Could not get that business user"}

    def update_business_profile(
        self, business_uuid: UUID, updated_profile: UserIn, user_uuid: UUID
    ) -> BusinessOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET
                            role = %s,
                            img_url = %s,
                            display_name = %s,
                            description = %s,
                            hours = %s,
                            location = %s
                        WHERE uuid = %s
                        """,
                        (
                            updated_profile.role,
                            updated_profile.img_url,
                            updated_profile.display_name,
                            updated_profile.description,
                            updated_profile.hours,
                            updated_profile.location,
                            business_uuid,
                        ),
                    )
                    updated_business = self.get_business_profile(business_uuid)
                    return updated_business
        except Exception as e:
            print(e)
            return {"message": "Could not update business user."}

    def get_owner_role(self, business_uuid: UUID4) -> int:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT role FROM users WHERE uuid = %s
                        """,
                        [business_uuid],
                    )
                    return result.fetchone()[0]
        except Exception as e:
            print(e)

    def get_businesses_by_location(
        self, location: str
    ) -> List[SimpleBusinessResponse]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        uuid,
                        display_name,
                        img_url,
                        description,
                        hours,
                        location
                        FROM users
                        WHERE location = %s AND role = 1
                        """,
                        [location],
                    )
                    businesses = []
                    for business_user in result.fetchall():
                        business = SimpleBusinessResponse(
                            uuid=business_user[0],
                            name=business_user[1],
                            img_url=business_user[2],
                            description=business_user[3],
                            hours=business_user[4],
                            location=business_user[5],
                        )
                        businesses.append(business)
                    return businesses
        except Exception as e:
            print(e)
            return [{"message": "Could not retrieve businesses by location."}]
