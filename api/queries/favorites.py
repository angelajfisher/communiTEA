from models.favorites import FavIn, FavOut
from pydantic import UUID4
from typing import List
from queries.pool import pool
import uuid as uuid_lib


class FavRepository:
    def create(self, fav: FavIn) -> FavOut:
        """
        Adds a new favorite tea association to the database.
        Requires a valid FavIn instance.
        Returns a FavOut instance with the newly created data.
        If provided tea or user UUIDs are not in database, returns None.
        """
        try:
            new_uuid = uuid_lib.uuid4()
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO favorites(
                            uuid,
                            user_uuid,
                            tea_uuid
                        )
                        VALUES (%s, %s, %s)
                        RETURNING uuid
                        """,
                        [new_uuid, fav.user_uuid, fav.tea_uuid],
                    )
                    uuid = result.fetchone()[0]
                    old_data = fav.dict()
                    return FavOut(uuid=uuid, **old_data)
        except Exception as e:
            print(e)

    def get(self, fav_id: UUID4) -> FavOut:
        """
        Retrieves a specific favorite from the database from its UUID.
        Requires a valid UUID.
        Returns a FavOut instance with the data from the relevant row.
        If none found, returns None.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT * FROM favorites
                        WHERE uuid = (%s)
                        """,
                        [fav_id],
                    )
                    fav = result.fetchone()
                    if fav is None:
                        return None
                    return FavOut(
                        uuid=fav[0],
                        user_uuid=fav[1],
                        tea_uuid=fav[2],
                    )
        except Exception as e:
            print(e)

    def get_user_favs(self, user_uuid) -> List[FavOut]:
        """
        Retrieves the favorites associated with a given user.
        Requires a valid user UUID.
        Returns a list of FavOuts.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM favorites
                        WHERE user_uuid = %s
                        """,
                        [user_uuid],
                    )

                    fav_list = []
                    for record in result.fetchall():
                        fav = FavOut(
                            uuid=record[0],
                            user_uuid=record[1],
                            tea_uuid=record[2],
                        )
                        fav_list.append(fav)
                    return fav_list
        except Exception as e:
            print(e)
            return None

    def delete(self, fav_id: UUID4) -> bool:
        """
        Removes a row from the favorites table corresponding to the given ID.
        Requires a valid UUID.
        Returns True upon successful deletion.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM favorites
                        WHERE uuid = %s
                        """,
                        [fav_id],
                    )
                    return True
        except Exception as e:
            print(e)

    def check_existence(self, fav: FavIn) -> bool:
        """
        Checks the database for an entry matching the given information.
        Requires a valid FavIn instance.
        Returns True if a row exists with the given values, False if not found.
        """
        user_uuid = fav.user_uuid
        tea_uuid = fav.tea_uuid
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT uuid FROM favorites
                    WHERE user_uuid = %s
                    AND tea_uuid = %s
                    """,
                    [user_uuid, tea_uuid],
                )
                if result.fetchone() is None:
                    return False
                return True
