from pydantic import BaseModel, UUID4


class ReviewIn(BaseModel):
    rating: int
    comment: str


class ReviewOut(ReviewIn):
    uuid: UUID4
    business: UUID4
    author: UUID4
