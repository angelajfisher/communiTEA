from pydantic import BaseModel, UUID4, Field
from typing import Optional


class UserModel(BaseModel):
    username: str
    img_url: Optional[str]
    display_name: str
    description: Optional[str]
    hours: Optional[str]
    location: Optional[str]


class UserIn(UserModel):
    role: Optional[int] = Field(default=0, ge=0, le=1)
    password: str


class UserOut(UserModel):
    role: int
    uuid: UUID4


class UserOutWithPassword(UserOut):
    hashed_password: str


class LoginForm(BaseModel):
    username: str
    password: str
