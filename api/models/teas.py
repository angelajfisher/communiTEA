from pydantic import BaseModel, UUID4
from typing import Optional


class TeaIn(BaseModel):
    name: str
    description: str
    img_url: Optional[str]
    published: Optional[bool] = False


class TeaOut(TeaIn):
    uuid: UUID4


class TeaOutWithLikes(TeaOut):
    likes: int


class DraftOut(BaseModel):
    tea_uuid: UUID4
    draft_uuid: UUID4
    name: str
    description: Optional[str]
    img_url: Optional[str]
