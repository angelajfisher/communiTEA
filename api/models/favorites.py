from pydantic import BaseModel, UUID4


class FavIn(BaseModel):
    tea_uuid: UUID4
    user_uuid: UUID4


class FavOut(FavIn):
    uuid: UUID4
