from pydantic import BaseModel, UUID4
from typing import Optional


class BusinessModel(BaseModel):
    uuid: UUID4
    name: str
    img_url: Optional[str]


class SimpleBusinessResponse(BusinessModel):
    description: Optional[str]
    hours: Optional[str]
    location: Optional[str]


class BusinessOut(SimpleBusinessResponse):
    username: str
    role: int


class MiniBusiness(BaseModel):
    name: str
    img_url: str
    uuid: UUID4
