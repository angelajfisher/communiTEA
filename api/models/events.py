from pydantic import BaseModel, UUID4
from typing import Optional
from datetime import date, time


class EventIn(BaseModel):
    title: str
    location: str
    date: date
    time: time
    description: str
    img_url: Optional[str]


class EventOut(EventIn):
    uuid: UUID4
    creator: UUID4
